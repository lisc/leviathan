package fr.irstea.lisc.leviathan;



import javax.swing.JFrame;

/**
 * Classe d'instanciation d'une interface de visualisation du lattice
 */
class GUI extends JFrame {

    Population mymodel;
    GridDrawer2 drawer;
    String titre;

    public GUI(Population mod, String title) {
        super();
        titre = title;
        mymodel = mod;
        setTitle(titre + " -- iter : " + (int) mymodel.getIteration());
        //drawer = new GridDrawer(((mod.scale-1)),((mod.scale-1))) ;
        //drawer = new GridDrawer(mymodel.sizeP*10,mymodel.sizeP*10, mymodel) ;
        //drawer = new GridDrawer(512,512,mymodel) ; // au lieu de 100, 100
        drawer = new GridDrawer2(mymodel.sizeP * 2, mymodel.sizeP * 2, mymodel); // au lieu de 100, 100
        setContentPane(drawer.getDisplay(mymodel));
        pack();
        setBounds(0, 0, 538, 572);
        setLocation(5, 5);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void update() {
        setTitle(titre + " -- iter : " + mymodel.getIteration());
        drawer.getDisplay(mymodel).repaint();
    }

    public void close() {
        dispose();
    }

}
