package fr.irstea.lisc.leviathan;


import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;

/*
 * This is a tool class dedicated to measure on network
 */
/**
 *
 * @author sylvie.huet
 */
public class Network {

    Individu[] myPop;
    String networkName;
    int sizeP;
    int nbCompConnexes;
    float averageDegree;
    float minDegree;
    float maxDegree;
    int[] degreeDistrib;
    float averageShortPathLength;
    float minShortPathLength;
    float maxShortPathLength;
    double averageClusteringCoeff;
    double minClusteringCoeff;
    double maxClusteringCoeff;
    int nbLinks;
    int nbRecLinks;

    public Network(Individu[] pop, String netName) {
        myPop = pop;
        sizeP = myPop.length;
        networkName = netName;
    }

    /**
     * Calcul et édition des propriétés initiales du réseau
     */
    public void calculAndEditProperties(MonModele model, String patern) {
        averageDegree = 0;
        minDegree = 0;
        maxDegree = 0;
        averageShortPathLength = 0;
        minShortPathLength = 0;
        maxShortPathLength = 0;
        averageClusteringCoeff = 0;
        minClusteringCoeff = 0;
        maxClusteringCoeff = 0;
        nbComposantConnexes();// in fact it is nb composantes connexes
        nbLinks = 0;
        nbRecLinks = 0;
        degreeInfo();
        if (nbCompConnexes == 1) {
            shorterPathLenghtDistribution();
        }
        clusteringCoeff();
        links();
        printPropertiesSynthesis(model, patern);
        //tailleFrontiere();
    }

    public void printPropertiesSynthesis(MonModele mo, String pattern) {
        mo.ps9.print(networkName + ";");
        mo.ps9.print(nbCompConnexes + ";");
        mo.ps9.print(new Float(averageDegree).toString().replace(".", ",") + ";");
        mo.ps9.print(new Float(minDegree).toString().replace(".", ",") + ";");
        mo.ps9.print(new Float(maxDegree).toString().replace(".", ",") + ";");
        mo.ps9.print(new Float(averageShortPathLength).toString().replace(".", ",") + ";");
        mo.ps9.print(new Float(minShortPathLength).toString().replace(".", ",") + ";");
        mo.ps9.print(new Float(maxShortPathLength).toString().replace(".", ",") + ";");
        mo.ps9.print(new Double(averageClusteringCoeff).toString().replace(".", ",") + ";");
        mo.ps9.print(new Double(minClusteringCoeff).toString().replace(".", ",") + ";");
        mo.ps9.print(new Double(maxClusteringCoeff).toString().replace(".", ",") + ";");
        mo.ps9.print(nbLinks + ";");
        mo.ps9.print(nbRecLinks + ";");
        mo.ps9.print(pattern + ";");
        //System.err.println("Degré distribution ");
        for (int i = 0; i < degreeDistrib.length; i++) {
            mo.ps9.print(degreeDistrib[i] + ";");
        }
        mo.ps9.println();
    }

    public void degreeInfo() {
        int endDistrib = sizeP + 1;
        //int endDistrib = (int) ((float) sizeP / 2.0) + 1;
        degreeDistrib = new int[endDistrib + 1]; // +1 for the 0 modality
        averageDegree = 0.0f;
        minDegree = sizeP;
        maxDegree = 0.0f;
        for (int i = 0; i < sizeP; i++) {
            if (myPop[i].voisinsPos.size() > endDistrib) {
                degreeDistrib[endDistrib]++;
            } else {
                degreeDistrib[myPop[i].voisinsPos.size()]++;
            }
            averageDegree = averageDegree + (float) myPop[i].voisinsPos.size();
            if ((float) myPop[i].voisinsPos.size() > maxDegree) {
                maxDegree = (float) myPop[i].voisinsPos.size();
            }
            if ((float) myPop[i].voisinsPos.size() < minDegree) {
                minDegree = (float) myPop[i].voisinsPos.size();
            }
        }
        averageDegree = averageDegree / (float) sizeP;
    }

    public void links() {
        nbLinks = 0;
        nbRecLinks = 0;
        for (int i = 0; i < sizeP; i++) {
            nbLinks = nbLinks + myPop[i].voisinsPos.size();
            for (int j = 0; j < myPop[i].voisinsPos.size(); j++) {
                if (((Individu) myPop[i].voisinsPos.get(j)).voisinsPos.contains(myPop[j])) {
                    nbRecLinks++;
                }
            }
        }
    }

    public void shorterPathLenghtDistribution() {
        // CALCUL DE SHORTER PATH LENGTH DISTRIBUTION***
        int[][] distanceCouples = new int[sizeP][sizeP];
        int i, j, z = 0;
        for (i = 0; i < sizeP; i++) {
            Arrays.fill(distanceCouples[i], 0);
        }
        int dist = 0;
        int distMax = 0;
        int ind = 0;
        int vois = 0;
        boolean pasFini = true;
        boolean nextI = false;
        int nbDejaVu = 0;
        ArrayList voisinARegarderEnCours = new ArrayList();
        ArrayList voisinARegarderSuivant = new ArrayList();
        boolean[] dejaVu = new boolean[sizeP];
        for (i = 0; i < sizeP; i++) {
            nextI = true;
            // On ne fait pas le calcul si tous les j sauf j = i ont une
            // distanceCouple différentes de 0
            for (j = 0; j < i; j++) {
                if (distanceCouples[i][j] == 0) {
                    nextI = false;
                }
            }
            for (j = i + 1; j < sizeP; j++) {
                if (distanceCouples[i][j] == 0) {
                    nextI = false;
                }
            }
            if (!nextI) {
                Arrays.fill(dejaVu, false);
                dejaVu[i] = true;
                nbDejaVu++;
                voisinARegarderEnCours.add(new Integer(i));
                dist = 1;
                pasFini = true;
                while (pasFini) {
                    for (j = 0; j < voisinARegarderEnCours.size(); j++) {
                        ind = ((Integer) (voisinARegarderEnCours.get(j))).intValue();
                        for (z = 0; z < myPop[ind].tailleVoisinagePos; z++) {
                            vois = ((Individu) (myPop[ind].voisinsPos.get(z))).getId();
                            if (dejaVu[vois] == false) {
                                distanceCouples[i][vois] = dist;
                                voisinARegarderSuivant.add(new Integer(vois));
                                dejaVu[vois] = true;
                                nbDejaVu++;
                                if (nbDejaVu == sizeP - 1) {
                                    pasFini = false;
                                    if (dist > distMax) {
                                        distMax = dist;
                                    }
                                }
                            }
                        }
                    }
                    dist++;
                    voisinARegarderEnCours = new ArrayList();
                    voisinARegarderEnCours = voisinARegarderSuivant;
                    voisinARegarderSuivant = new ArrayList();
                    if (voisinARegarderEnCours.size() == 0) {
                        pasFini = false;
                        if (dist > distMax) {
                            distMax = dist - 1;
                        }
                    }
                }
            }
        }
        // Récupération de la distribution des distances min (shorther path length)
        int[] distances = new int[distMax + 1];
        Arrays.fill(distances, 0);
        averageShortPathLength = 0.0f;
        maxShortPathLength = 0;
        minShortPathLength = Integer.MAX_VALUE;
        int nbDistance = 0;
        for (i = 0; i < sizeP - 1; i++) {
            for (j = i + 1; j < sizeP; j++) {
                distances[distanceCouples[i][j]]++;
                averageShortPathLength = averageShortPathLength + (float) distanceCouples[i][j];
                if (minShortPathLength > distanceCouples[i][j]) {
                    minShortPathLength = distanceCouples[i][j];
                }
                if (maxShortPathLength < distanceCouples[i][j]) {
                    maxShortPathLength = distanceCouples[i][j];
                }
                nbDistance++;
            }
        }
        averageShortPathLength = averageShortPathLength / (float) nbDistance;
        //System.err.println("Distribution des distances deux à deux");
        int p = 0;
        for (i = 0; i < distMax; i++) {
            p = i + 1;
            //System.err.print(p + "\t");
        }
        //System.err.println();
        float[] distancesPartPop = new float[distMax + 1];
        for (i = 0; i < distMax; i++) {
            distancesPartPop[i + 1] = distances[i + 1] / ((float) sizeP * ((float) sizeP - 1f) / 2);
            //System.err.print(distancesPartPop[i + 1] + "\t");
        }
        //System.err.println();
    }

    public void clusteringCoeff() {
        // CALCUL DE COEFFICIENT DE CLUSTERING C****
        int nbLiensPossiblesEntreVoisins = 0;
        int nbLiensReelsEntreVoisins = 0;
        int l, k = 0;
        Individu indiv;
        double[] coeffClustLocal = new double[sizeP];
        Arrays.fill(coeffClustLocal, 0.0);
        for (int i = 0; i < sizeP; i++) {
            nbLiensPossiblesEntreVoisins = (myPop[i].tailleVoisinagePos * (myPop[i].tailleVoisinagePos - 1)) / 2;
            nbLiensReelsEntreVoisins = 0;
            // mesure du nombre de liens réels entre les voisins ;
            for (int j = 0; j < myPop[i].tailleVoisinagePos; j++) {
                indiv = (Individu) (myPop[i].voisinsPos.get(j));
                for (k = j + 1; k < myPop[i].tailleVoisinagePos; k++) {
                    for (l = 0; l < indiv.tailleVoisinagePos; l++) {
                        if (indiv.voisinsPos.get(l) == myPop[i].voisinsPos.get(k)) {
                            nbLiensReelsEntreVoisins++;
                            if (nbLiensReelsEntreVoisins > nbLiensPossiblesEntreVoisins) {
                                System.err.println(" CRASSHHH ");
                            }
                        }
                    }
                }
            }
            if (nbLiensReelsEntreVoisins > 0.0) {
                coeffClustLocal[i] = (double) nbLiensReelsEntreVoisins / (double) nbLiensPossiblesEntreVoisins;
            } else {
                coeffClustLocal[i] = 0;
            }
            // Débuggage
            if (coeffClustLocal[i] > 1) {
                System.err.println("	cc local de i = " + i + " : " + coeffClustLocal[i]);
                System.err.println("	Liens " + nbLiensReelsEntreVoisins + " " + nbLiensPossiblesEntreVoisins);
                System.err.println("	taille voisinage " + myPop[i].tailleVoisinagePos);
                // Edition des voisins de i et des voisins des voisins de i
                for (int j = 0; j < myPop[i].tailleVoisinagePos; j++) {
                    indiv = (Individu) (myPop[i].voisinsPos.get(j));
                    System.err.println("voisin de i : " + indiv.getId());
                    for (l = 0; l < indiv.tailleVoisinagePos; l++) {
                        System.err.println("voisin de voisin de i : " + ((Individu) indiv.voisinsPos.get(l)).getId());
                    }
                }
            }
        }

        // Calcul du coefficient de clustering moyen
        double sommeCoeffClust = 0.0;
        minClusteringCoeff = Double.MAX_VALUE;
        maxClusteringCoeff = Double.MIN_VALUE;
        for (int i = 0; i < sizeP; i++) {
            sommeCoeffClust = sommeCoeffClust + coeffClustLocal[i];
            if (coeffClustLocal[i] < minClusteringCoeff) {
                minClusteringCoeff = coeffClustLocal[i];
            } // Récup Min
            if (coeffClustLocal[i] > maxClusteringCoeff) {
                maxClusteringCoeff = coeffClustLocal[i];
            } // Récup Max
        }
        averageClusteringCoeff = sommeCoeffClust / (double) sizeP;
    }

    /**
     * Calcul du nombre de cluster dans la population (clusters de taille
     * supérieure à 3 individus)
     */
    public void nbComposantConnexes() {
        int nbClustSupUnPourCent = 0;
        int nbClustSupUnDemiPourCent = 0;
        int nbClustSupDeuxPourCent = 0;
        ArrayList tailleClust = new ArrayList();
        int objet = 0;
        nbCompConnexes = 0;

        int unPourMille = (int) (sizeP * 0.001f); // limite à un pour mille
        int unDemiPourCent = (int) (sizeP * 0.005f); // limite à un pour mille
        int unPourCent = (int) (sizeP * 0.01f); // limite à un pour mille
        int deuxPourCent = (int) (sizeP * 0.02f); // limite à un pour mille
        int dixPourCent = (int) (sizeP * 0.1f); // limite à un pour mille
        int quatrePourCent = (int) (sizeP * 0.04f); // limite à un pour mille
        float epsilon = 0.02f; // au lieu de 0.2f ;

        int[] aParcourir = new int[sizeP];
        ArrayList clustEnCours = new ArrayList();
        int i = 0;
        int j = 0;
        for (i = 0; i < sizeP; i++) {
            aParcourir[i] = i + 1;
        }
        int compt = 0;
        int comptTailleCluster = 0;
        for (i = 0; i < sizeP; i++) {
            comptTailleCluster = 0;
            if (aParcourir[i] > 0) {
                clustEnCours.add(new Integer(i));
                compt++;
                aParcourir[i] = 0;
                int k = 0;
                comptTailleCluster++;
                while (!clustEnCours.isEmpty()) {
                    for (j = 0; j < sizeP; j++) {
                        if (aParcourir[j] > 0) {
                            //if (distance(myPop[((Integer) (clustEnCours.get(k))).intValue()], myPop[j], objet) < epsilon) {
                            if (myPop[((Integer) (clustEnCours.get(k))).intValue()].voisinsPos.contains(myPop[j])) {
                                aParcourir[j] = 0;
                                clustEnCours.add(new Integer(j));
                                comptTailleCluster++;
                                compt++;
                                /*
                                 * if (note) { if (nbClust == 0) { cluster[j] =
                                 * true; } }
                                 *
                                 */
                            }
                        }
                    }
                    clustEnCours.remove(k);
                }
                //note = false;
                nbCompConnexes++;
                nbClustSupUnPourCent++;
                nbClustSupUnDemiPourCent++;
                nbClustSupDeuxPourCent++;
                ///*
                if (comptTailleCluster < unPourMille) {
                    nbCompConnexes--;
                }
                if (comptTailleCluster < unPourCent) {
                    nbClustSupUnPourCent--;
                }
                if (comptTailleCluster < unDemiPourCent) {
                    nbClustSupUnDemiPourCent--;
                }
                if (comptTailleCluster < deuxPourCent) {
                    nbClustSupDeuxPourCent--;
                }
                tailleClust.add(new Integer(comptTailleCluster));
            }
            if (compt == sizeP) {
                i = sizeP;
            }
        }
    }
    /*
     * public void tailleFrontiere() { // DETERMINATION DE TAILLE DE LA
     * FRONTIERE pour différentes conditions de minorité clustérisée float
     * tauxMinCluster[] = {0.1f, 0.2f, 0.3f, 0.4f, 0.5f}; float partIndivRecense
     * = 0f; float partIndivFrontiere = 0f; float partIndivFrontierePlusPret,
     * partIndivFrontierePlusLoin = 0f; float estimAccroiss = 0.0f;
     * //System.out.println("Proportion frontière pour différents taux de
     * minorité négative initiales :") ; for (i = 0; i < tauxMinCluster.length;
     * i++) { //System.out.print(tauxMinCluster[i]+";") ; }
     * //System.out.println("") ; for (i = 0; i < tauxMinCluster.length; i++) {
     * partIndivRecense = 0f; for (j = 0; j < distMax; j++) { partIndivRecense =
     * partIndivRecense + distancesPartPop[j + 1]; if (partIndivRecense >
     * tauxMinCluster[i]) { partIndivRecense = partIndivRecense -
     * distancesPartPop[j + 1]; // si i = 0, cas special ou on ne dépasse pas la
     * distance 1 if (j == 0) { partIndivFrontiere = tauxMinCluster[i]; } else
     * {// si i > 0 partIndivFrontierePlusPret = distancesPartPop[j];
     * partIndivFrontierePlusLoin = tauxMinCluster[i] - partIndivRecense;
     * estimAccroiss = distancesPartPop[j + 1] / distancesPartPop[j];
     * partIndivFrontiere = partIndivFrontierePlusLoin +
     * partIndivFrontierePlusPret - (partIndivFrontierePlusLoin /
     * estimAccroiss); System.out.print(partIndivFrontiere + ";"); } j =
     * distMax; // on arrête } } } }
     */
}
