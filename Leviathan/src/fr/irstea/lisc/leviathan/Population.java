package fr.irstea.lisc.leviathan;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.Timer;

/**
 * Classe instanciant une myPop qui se met � jour et d�tient tous les �lements
 * relatifs � la fonction de d�cision last changes: 29.02.2013 change in order
 * creating typeNetwork = 1 (en groupe) with everyone having her own group
 * members as neighbours + other neighbours from other group with a probability
 * saved in noise (resM saved the desired number of groups) - the size of the
 * group is constant ie uniform - each individual gossip only of people members
 * of its own neighbourhood.
 */
public class Population {

    String expeLotName;
    boolean[] cluster;
    /**
     * Boolean indiquant qu'on visualise le lattice ou non (si lattice)
     */
    public boolean visu = false;
    long iteration;
    // Variable to make the pattern diagnosis
    float minReputation;
    float maxReputation;
    int indivMaxReput;
    int indivMinReput;
    float maxOpOnMaxReput;
    float minOpOnMaxReput;
    float maxOpOnMinReput;
    int nbCrisis;
    int nbElite;
    int nbHierarchy;
    int nbSmallWorld;
    int nbDominance;
    boolean patternSynthesis = false;
    float minOp;
    float maxOp;
    float minOpSoi;
    float maxOpSoi;
    int nbNegOpinion;
    /**
     * The most loved having a positive reputation and the previous one
     */
    int leader = -1;
    int previousLeader = -1;
    int secondLeader = -1;
    int previousSecondLeader = -1;
    int last = -1;
    int previousLast = -1;
    int fixed1 = -1; //Parameter used to fix the leader on whom we are computing avalanches
    int fixed2 = -1; //As before, but for the second highest guy

    public long getIteration() {
        return iteration;
    }

    public void setIteration(long iteration) {
        this.iteration = iteration;
    }
    /**
     * To measure the self-esteem biais
     */
    float pasDis;
    int size;
    //float[] deltaSelfOpReput;
    double[] deltaSelfOpReput;
    int[] nbIndiv;
    float[] selfEsteem;
    float[] selfEsteem0;
    float[] selfEsteem1;
    float[] reput;
    float[] reput0;
    float[] reput1;
    boolean v0 = true;
    boolean c0 = true;
    boolean patternDiagnosis = false;
    boolean writeDataBase = true;
    float widthClasses = 0.10f; //float for popularity histogram;
    int nbSelfLovers = 0;
    int nbLovedByAll = 0;
    int nbHatedByAll = 0;

    public boolean getVisu() {
        return this.visu;
    }

    public void setVisu(boolean value) {
        visu = value;
    }
    GUI monGui;
    long previousIterat;
    long previousWriting;
    /**
     * Boolean permettant d'activer le mode d'échange rencontre deux à deux sur
     * tous les individus de la myPop
     */
    public boolean rencontre2a2 = false;

    public boolean getRencontre2a2() {
        return this.rencontre2a2;
    }

    public void setRencontre2a2(boolean value) {
        rencontre2a2 = value;
    }
    /**
     * Boolean permettant d'activer le mode d'échange rencontre deux à deux sur
     * deux individus de la myPop liés au sein d'un réseau
     */
    public boolean rencontre2a2connectes = false;

    public boolean getRencontre2a2connectes() {
        return this.rencontre2a2connectes;
    }

    public void setRencontre2a2connectes(boolean value) {
        rencontre2a2connectes = value;
    }
    /**
     * Attributes for the Leviathan model
     */
    Parameters parameters;
    int sizeP;
    Individu[] myPop;
    Random myRand;
    /**
     * Average, min and max reputation
     */
    float averageOpinion;
    //float minReputation;
    //float maxReputation;
    float secondMaxReputation;
    float diff;
    float dominance;
    MonModele mod; //To write two different output files

    /**
     * Give the reputation of the individual
     */
    public float getReputation(int indiv) {
        float reput = 0.0f;
        myPop[indiv].nbKnownBy = 0;
        for (int i = 0; i < sizeP; i++) {
            if (myPop[i].knows(indiv)) {
                if (i != indiv) {
                    reput = reput + myPop[i].opinion[indiv];
                    myPop[indiv].nbKnownBy++;
                    //System.err.println("reput " + reput + " ddd " + myPop[indiv].nbKnownBy + " indiv " + indiv + " i " + i + " op " + myPop[i].opinion[indiv]);
                }
            }
        }
        if (myPop[indiv].nbKnownBy != 0) {
            reput = reput / (float) myPop[indiv].nbKnownBy;
        }
        return reput;
    }
    
    /**
     * Return the opinion value normalized in [0,1]
     * @param i
     * @return 
     */
    public static float normalizeOpinion(float value) {
        value = Math.max(value, -1); // trailing values < -1
        value = Math.min(value, 1); // trailing values > 1
        value = (value + 1) / 2; // scaling between 0 and 1
        return value;
    }

    public void findMaxReputation() {
        float reputation;
        //nbLoved = 0;
        maxReputation = -2;
        for (int i = 0; i < sizeP; i++) {
            reputation = getReputation(myPop[i].getId());
            if (reputation > maxReputation) {
                maxReputation = reputation;
                indivMaxReput = i;
                //mostLoved = i;
                leader = i;
                /*
                 * if (reputation > -1) { leader = i; }
                 */
            }
            /*
             * if (reputation > l) { nbLoved++; }
             */
        }
    }

    public void computeStatistics() {
        nbSelfLovers = 0;
        nbLovedByAll = 0;
        nbHatedByAll = 0;
        for (int i = 0; i < sizeP; i++) {
            if (myPop[i].opinion[i] > 0.0f) {
                nbSelfLovers++;
            }
            if (getReputation(i) > 0.0) {
                nbLovedByAll++;
            }
            if (getReputation(i) < 0.0) {
                nbHatedByAll++;
            }
        }
    }

    public void findSecondMaxReputation() {
        float reputation;
        secondMaxReputation = -1;
        for (int i = 0; i < sizeP; i++) {
            reputation = getReputation(myPop[i].getId());
            if (reputation > secondMaxReputation) {
                if (i != leader) {
                    secondMaxReputation = reputation;
                    secondLeader = i;
                }
            }
        }
    }

    public void findMinReputation() {
        float reputation;
        minReputation = 2;
        for (int i = 0; i < sizeP; i++) {
            reputation = getReputation(myPop[i].getId());
            if (reputation < minReputation) {
                minReputation = reputation;
                indivMinReput = i;
                last = i;
            }
        }
    }

    public void findAverageOpinion() {
        averageOpinion = 0;
        int count = 0;
        for (int i = 0; i < sizeP; i++) {
            for (int j = 0; j < sizeP; j++) {
                //if (myPop[i].opinion[j] != -5.0f) {
                if (myPop[i].knows(j)) {
                    averageOpinion = averageOpinion + myPop[i].opinion[j];
                    count++;
                }
            }
        }
        averageOpinion = averageOpinion / (float) (count);
        /*
         * for (int i = 0; i < sizeP; i++) { for (int j = 0; j <
         * myPop[i].acquaintances.size(); j++) { average2 = average2 +
         * myPop[i].opinion[((Integer)
         * myPop[i].acquaintances.get(j)).intValue()]; count2++; } } average2 =
         * average2 / (float) (count2); System.err.println("Contatore1 " + count
         * + " contatore2 " + count2); System.err.println("Media 1 " +
         * averageOpinion + " media 2 " + average2); for (int i = 0; i < sizeP;
         * i++) { System.err.println("Indiv " + i + " knows " +
         * myPop[i].acquaintances); }
         */
    }

    /**
     * Calculate dominance
     */
    public float getDominance() {
        findMaxReputation();
        findMinReputation();
        findAverageOpinion();
        dominance = maxReputation - averageOpinion - ((maxReputation - minReputation) / 2);
        //System.err.println("dominance " + dominance + " max " + maxReputation + " min " + minReputation + " average " + averageOpinion);
        return dominance;
    }

    /**
     * Constructor for demo purpose
     *
     * @param parameters
     */
    public Population(Parameters parameters) {
        this.parameters = parameters;
        this.writeDataBase = false;
        mod = null;
        monGui = null;
        sizeP = parameters.getPopSize();
        myRand = new Random();
        setVisu(true);
        long stuf = (long) (12345 * Math.random());
        myRand.setSeed(new long[]{stuf, 12345, 12345, 12345, 12345, 12345});
        initMyPop();
        initSelfEsteemBiais();
    }

    /**
     * Constructor for the version with a network and a type of discussion
     * (varying from whom individuals talk about)
     */
    public Population(Parameters parameters, MonModele model) {
        this.parameters = parameters;
        mod = model;
        monGui = null;
        sizeP = parameters.getPopSize();
        myRand = new Random();
        setVisu(false);
        long stuf = (long) (12345 * Math.random());
        myRand.setSeed(new long[]{stuf, 12345, 12345, 12345, 12345, 12345});
        initMyPop();
        initSelfEsteemBiais();
        printParametersForPs1_3_4_5();
        iteration();
        /*
         * initmyPop(freqSauvegarde, typeDiscus, typeRes, resM, graine);
         * //initSelfEsteemBiais(); iter(nbIter, graine, freqSauvegarde);
         *
         */

    }

    /**
     * Constructor for the version doing lot of experimentations and writing
     * files for databases
     */
    public Population(String expe, Parameters parameters, MonModele model) {
        this.parameters = parameters;
        monGui = null;
        mod = model;
        sizeP = parameters.getPopSize();
        myRand = new Random();
        setVisu(true);
        expeLotName = expe;
        minReputation = 1.0f;
        maxReputation = -1.0f;
        indivMaxReput = -1;
        indivMinReput = -1;
        maxOpOnMaxReput = -1.0f;
        minOpOnMaxReput = 1.0f;
        maxOpOnMinReput = -1.0f;
        nbCrisis = 0;
        nbElite = 0;
        nbHierarchy = 0;
        nbSmallWorld = 0;
        nbDominance = 0;
        long stuf = (long) (12345 * Math.random());
        myRand.setSeed(new long[]{stuf, 12345, 12345, 12345, 12345, 12345});
        //System.err.println("TypeRes " + typeRes + " resM(NbGroupes) " + resM + " noise(pLinkOthers) " + noise);
        initMyPop();
        monGui = new GUI(this, "Leviathan Model");
        monGui.update();
        //initSelfEsteemBiais();
        Timer timer = new Timer(50, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                monGui.update();
            }

        });
        timer.start();
        iteration();
    }

    public void initSelfEsteemBiais() {
        pasDis = 0.04f;
        size = (int) (2.0f / pasDis);
        //deltaSelfOpReput = new float[size];
        deltaSelfOpReput = new double[size];
        nbIndiv = new int[size];
        java.util.Arrays.fill(deltaSelfOpReput, 0.0);
        //java.util.Arrays.fill(deltaSelfOpReput, 0.0f);
        java.util.Arrays.fill(nbIndiv, 0);
        selfEsteem = new float[size];
        java.util.Arrays.fill(selfEsteem, 0.0f);
        reput = new float[size];
        java.util.Arrays.fill(reput, 0.0f);
        selfEsteem0 = new float[size];
        java.util.Arrays.fill(selfEsteem0, 0.0f);
        reput0 = new float[size];
        java.util.Arrays.fill(reput0, 0.0f);
        selfEsteem1 = new float[size];
        java.util.Arrays.fill(selfEsteem1, 0.0f);
        reput1 = new float[size];
        java.util.Arrays.fill(reput1, 0.0f);
    }
    /*
     * public void initLevPop(int freqSauvegarde) { setRencontre2a2(true); myPop
     * = new Individu[sizeP]; float omega = w; float rho = ro; float impress =
     * delta; int nbInter = nbInterloc; int nbIndivDif = 0; // individuals
     * different from the other for (int i = 0; i < sizeP; i++) { if (i <
     * nbIndivDif) { impress = 0.5f; } myPop[i] = new Individu(this, omega,
     * nbInter, rho, impress, i); } //ecritResultat(0, false, 0,
     * freqSauvegarde); if (getVisu()) { monGui = new GUI(this, "Leviathan
     * Model", 0); monGui.update(0); try { } catch (Exception e) {
     * System.err.println("Il y a un probleme avec le Thread");
     * e.printStackTrace(); } } }
     */

    /**
     * Initialisation de la myPop (des individus)
     */
    public void initMyPop() {
        cluster = new boolean[sizeP];
        java.util.Arrays.fill(cluster, false);
        ArrayList[] reseau = new ArrayList[1];
        myPop = new Individu[sizeP];
        float omega = parameters.getWe();
        float rho = parameters.getR();
        float impress = parameters.getDelt();
        int nbInter = parameters.getKa();
        for (int i = 0; i < sizeP; i++) {
            myPop[i] = new Individu(this, omega, nbInter, rho, impress, i);
            if (i < sizeP / 2) {
                myPop[i].rencontrable = true;
            } else {
                myPop[i].rencontrable = false;
            }
        }
        // initialisation du mode de discussion et du réseau de voisinage
        switch (parameters.getTypeDiscus()) {
            case 1:
                setRencontre2a2(true);
                break;
            case 2:
                reseau = initReseau(parameters.getTypeRes(), parameters.getResM(), parameters.getNoise(), parameters.getGraine());
                for (int i = 0; i < sizeP; i++) {
                    myPop[i].setVoisins(reseau[i]);
                }
                setRencontre2a2connectes(true);
                break;
            default:
                break;
        }

    }

    public void iteration() {
        long nbIterat = (long) parameters.getNbIter() * ((long) sizeP / (long) 2);
        for (long i = 1; i <= nbIterat; i++) {
            iteration_step(i);
        }
        writingPatternSynthesis(getIteration());
    }

    public void iteration_step(long i) {
        long iterat = (long) (i / ((long) sizeP / (long) 2));
        setIteration(iterat);
        if (getRencontre2a2()) {
            rencontreDeuxADeux();
        } else {
            if (getRencontre2a2connectes()) {
                rencontreDeuxADeuxConnectes();
            }
        }
        String pat = new String();
        if (patternSynthesis) {
            if (previousIterat != iterat) {
                previousIterat = iterat;
                if (parameters.getFreqSauvegarde() > 0 && (iterat > (long) 10000) && (iterat % ((long) parameters.getFreqSauvegarde()) == (long) 0)) {
                    pat = patternDiagnosis();
                }
            }
        } else {
            if (iterat > (long) 0) {
                if (previousIterat != iterat) {
                    previousIterat = iterat;
                    if (previousWriting != iterat) {
                        if (iterat % (long) parameters.getFreqSauvegarde() == (long) 0 && iterat >= (long) 10000) {
                            if (writeDataBase) {
                                //writeForDataBase(iterat);
                                previousWriting = iterat;
                                characteriseReputation(expeLotName, parameters.getSimu(), parameters.getRep(), iterat);
                                computePosOpinionNetwork();
                                new Network(myPop, new String("havePosOpOf")).calculAndEditProperties(mod, patternDiagnosis());
                                /*
                                 * characteriseReputation(expeLotName,
                                 * parametres[0], parametres[1],
                                 * iterat);
                                 * computeEsteemOpinionNetwork(); new
                                 * Network(myPop, new
                                 * String("esteem")).calculAndEditProperties(mod,
                                 * patternDiagnosis());
                                 * characteriseReputation(expeLotName,
                                 * parametres[0], parametres[1],
                                 * iterat);
                                 * computeAffinityOpinionNetwork(); new
                                 * Network(myPop, new
                                 * String("Affinity")).calculAndEditProperties(mod,
                                 * patternDiagnosis());
                                 * characteriseReputation(expeLotName,
                                 * parametres[0], parametres[1],
                                 * iterat);
                                 * computeRecAffinityOpinionNetwork();
                                 * new Network(myPop, new
                                 * String("ReciprocalAffinity")).calculAndEditProperties(mod,
                                 * patternDiagnosis());
                                 * characteriseReputation(expeLotName,
                                 * parametres[0], parametres[1],
                                 * iterat);
                                 * computeIsEsteemedByOpinionNetwork();
                                 * new Network(myPop, new
                                 * String("IsEsteemedBy")).calculAndEditProperties(mod,
                                 * patternDiagnosis());
                                 */
                                writingOutputFile((long) iterat, false, false, false, true, false, true);
                            }
                        }
                    }
                }
            } else {
                if (previousIterat != iterat) {
                    previousIterat = iterat;
                    if (previousWriting != iterat) {
                        if (iterat % (long) parameters.getFreqSauvegarde() == (long) 0 && iterat >= (long) 10000) {
                            if (writeDataBase) {
                                writeForDataBase(iterat);
                                previousWriting = iterat;
                            }
                        }
                    }
                }
            }
        }
    }

    public void encounter(int i, int j) {
        int nbIndivTalk = 0;
        int tire = 0;
        // compute the difference of opinion of each interlocutor regarding ego and the other
        float delta = 0.0f;
        float dijj = 0.0f;
        float diji = 0.0f;
        float djij = 0.0f;
        float djii = 0.0f;
        // On pourrait éviter de retirer delta à chaque fois 
        myRand.nextDouble();
        ///*
        delta = -myPop[j].boundForImpression + ((float) myRand.nextDouble() * 2 * myPop[j].boundForImpression);
        dijj = myPop[j].opinion[j] + delta - myPop[i].opinion[j];
        delta = -myPop[j].boundForImpression + ((float) myRand.nextDouble() * 2 * myPop[j].boundForImpression);
        diji = myPop[j].opinion[i] + delta - myPop[i].opinion[i];
        delta = -myPop[i].boundForImpression + ((float) myRand.nextDouble() * 2 * myPop[i].boundForImpression);
        djij = myPop[i].opinion[j] + delta - myPop[j].opinion[j];
        delta = -myPop[i].boundForImpression + ((float) myRand.nextDouble() * 2 * myPop[i].boundForImpression);
        djii = myPop[i].opinion[i] + delta - myPop[j].opinion[i];

        // l'influence de J sur moi est lié à combien je le trouve mieux que moi ou le contraire et à combien il est sûr de lui (delta IJ est la certitude de J percu par I)
        double hij = computeCoefInfluence(i, j);
        double hji = computeCoefInfluence(j, i);
        // discuss (gossip) or assess themselve and the interlocutor during the encounter

        // annulation biais estime :
        //hij=1.0 ;
        //hji=1.0 ;
        myPop[i].opinion[i] = myPop[i].opinion[i] + myPop[i].ro * (float) hij * diji;
        myPop[i].opinion[j] = myPop[i].opinion[j] + myPop[i].ro * (float) hij * dijj;
        myPop[j].opinion[j] = myPop[j].opinion[j] + myPop[j].ro * (float) hji * djij;
        myPop[j].opinion[i] = myPop[j].opinion[i] + myPop[j].ro * (float) hji * djii;
        constraintInBound(i, i);
        constraintInBound(i, j);
        constraintInBound(j, j);
        constraintInBound(j, i);

        // select the individuals j are going to talk about to i
        nbIndivTalk = Math.min(myPop[j].k, myPop[j].acquaintances.size());
        int g = 0;
        int[] talkAboutJ = new int[nbIndivTalk];
        //float[] difTalkAboutJ = new float[nbIndivTalk];
        if (nbIndivTalk > 0) {
            while (g < nbIndivTalk) {
                if (myPop[j].acquaintances.size() == (sizeP - 1)) {
                    tire = (int) (myRand.nextInt(0, (sizeP - 1)));
                } else {
                    tire = (int) (myRand.nextInt(0, (myPop[j].acquaintances.size() - 1)));
                    tire = myPop[j].acquaintances.get(tire).intValue();
                }
                talkAboutJ[g] = tire;
                g++;
            }
        }
        // gossip - make them talking of those k individuals - gossip
        gossip(i, j, talkAboutJ, hij);
        //gossipSansBiais(i, j, talkAboutJ);

        // select the individuals i are going to talk to j
        //nbIndivTalk = Math.min(myPop[i].k, myPop[i].acquaintances.size());
        nbIndivTalk = Math.min(myPop[i].k, myPop[i].getVoisins().size());
        int[] talkAboutI = new int[nbIndivTalk];
        //float[] difTalkAboutI = new float[nbIndivTalk];
        if (nbIndivTalk > 0) {
            g = 0;
            while (g < nbIndivTalk) {
                if (myPop[i].acquaintances.size() == (sizeP - 1)) {
                    tire = (int) (myRand.nextInt(0, (sizeP - 1)));
                } else {
                    //tire = (int) (myRand.nextInt(0, (myPop[i].getVoisins().size() - 1)));
                    //tire = ((Individu)(myPop[i].voisins.get(tire))).id;
                    tire = (int) (myRand.nextInt(0, (myPop[i].acquaintances.size() - 1)));
                    tire = myPop[i].acquaintances.get(tire).intValue();
                }
                talkAboutI[g] = tire;
                g++;
            }
            g = 0;
        }
        // gossip - make them talking of those k individuals - gossip
        gossip(j, i, talkAboutI, hji);
        //gossipSansBiais(j, i, talkAboutI);

        // apply the vanity
        vanity(i, j, diji);
        vanity(j, i, djij);
        //vanity(i, j, diji, (float) hij); // fonction monde plus soft avec plus de positive opinion
        //vanity(j, i, djij, (float) hji); // fonction monde plus soft avec plus de positive opinion
    }

    public double computeCoefInfluence(int i, int j) {
        double h = 0.0;
        // l'influence de J sur moi dépend du niveau d'estime (combien je le trouve mieux que moi) que j'ai pour lui
        // 1. Fonction sigmoide de Leviathan JASSS 2013
        ///*
        if (parameters.getSig() == 0.0) {
            if ((myPop[i].opinion[j] - myPop[i].opinion[i]) >= 0.0f) {
                h = 1.0;
            } else {
                h = 0.0;
            }
        } else {
            h = (double) (1 / (1 + Math.exp(-((myPop[i].opinion[j] - myPop[i].opinion[i]) / parameters.getSig()))));
        }
        //*/
        // 2. Fonction linéaire
        /*
         double dist = (myPop[i].opinion[j]) - (myPop[i].opinion[i]); 
         if (dist < 0) { // individus j est moins estimé par i que i ne s'estime  lui-même 
         h = (double)sigma*(dist/4.0 + 0.5f); } 
         else { 
         // j est estimé par i 
         h = (dist/4.0 + 0.5f); 
         }
         */
        //System.err.println(" h "+h+" sigma "+sigma) ;//*/
        return h;
    }

    public void rencontreDeuxADeux() {
        int i = -1;
        int j = -1;
        i = -1;
        j = -1;
        // select two people talking
        if (sizeP > 2) {
            i = myRand.nextInt(0, (sizeP - 1));
            while (j == i || j == -1) {
                j = myRand.nextInt(0, (sizeP - 1));
            }
        } else {
            i = myRand.nextInt(0, (sizeP - 1));
            if (i == 0) {
                j = 1;
            } else {
                j = 0;
            }
        }
        if (i == j) {
            System.err.println(" i et j sont identiques ");
        }
        //System.err.println();
        //System.err.println(i + " is going to talk to " + j + " and viceversa. " + i + "'s opinion is " + myPop[i].opinion[j] + " and " + j + "'s opinions is " + myPop[j].opinion[i]);
        //if (myPop[i].opinion[i] == -5.0f) {
        if (myPop[i].acquaintances.size() == 0.0f) { // if it does not know anybody, then it does not know itself
            myPop[i].opinion[i] = 0.0f;
        }
        //if (myPop[i].opinion[j] == -5.0f) {
        if (!myPop[i].knows(j)) {
            if (parameters.getMem() > myPop[i].tailleVoisinage) {
                if (myPop[i].acquaintances.size() < parameters.getMem()) {
                    myPop[i].addAcquaintance(new Integer(j));
                    //System.err.println("Encounter: " + i + " has met " + j + " adding him");
                    //myPop[j].knownBy.add(new Integer(i));
                } else {
                    updateKnownIndiv(i, j, j);
                    //System.err.println("Encounter: " + i + " has added " + j + " removing someone");
                }
            }
        }
        //if (myPop[j].opinion[j] == -5.0f) {
        if (myPop[j].acquaintances.size() == 0.0f) {
            myPop[j].opinion[j] = 0.0f;
        }

        //if (myPop[j].opinion[i] == -5.0f) {
        if (!myPop[j].knows(i)) {
            if (parameters.getMem() > myPop[j].tailleVoisinage) {
                if (myPop[j].acquaintances.size() < parameters.getMem()) {
                    myPop[j].addAcquaintance(new Integer(i));
                    //System.err.println("Encounter2: " + j + " has met " + i + " adding him");
                    //myPop[i].knownBy.add(new Integer(j));
                } else {
                    updateKnownIndiv(j, i, i);
                    //System.err.println("Encounter2: " + j + " has added " + i + " removing someone");
                }
            }
        }
        /*
         * for (int t = 0; t < myPop[i].acquaintances.size(); t++) { if
         * (myPop[i].knows(i)) { System.err.println("i se connait lui-mêem : " +
         * i); } } for (int t = 0; t < myPop[j].acquaintances.size(); t++) { if
         * (myPop[j].knows(j)) { System.err.println("j se connait lui-mêem : " +
         * j); } }
         */
        encounter(i, j);
    }

    /**
     * Rencontre deux à deux des individus de la myPop (symétrique : i parle à j
     * et j parle à i) sous contraintes deux individus qui se rencontrent sont
     * liés au sein du réseau de voisinage
     */
    void rencontreDeuxADeuxConnectes() {
        int i = 0;
        int j = 0;
        i = myRand.nextInt(0, (sizeP - 1));
        while (myPop[i].tailleVoisinage == 0) { // i a au moins un voisin
            i = myRand.nextInt(0, (sizeP - 1));
        }
        if (myPop[i].opinion[i] == -5.0f) {
            myPop[i].opinion[i] = 0.0f;
        }
        j = ((Individu) (myPop[i].voisins.get((int) (myRand.nextInt(0, (myPop[i].tailleVoisinage - 1)))))).getId();
        if (myPop[i].opinion[j] == -5.0f) {
            myPop[i].opinion[j] = 0.0f;
        }
        Integer ind = new Integer(j);
        if (!myPop[i].acquaintances.contains(ind)) {
            myPop[i].addAcquaintance(ind);
        }
        if (myPop[j].opinion[j] == -5.0f) {
            myPop[j].opinion[j] = 0.0f;
        }
        if (myPop[j].opinion[i] == -5.0f) {
            myPop[j].opinion[i] = 0.0f;
        }
        ind = new Integer(i);
        if (!myPop[j].acquaintances.contains(ind)) {
            myPop[j].addAcquaintance(ind);
        }
        encounter(i, j);
    }

    /**
     * Rencontre deux à deux des individus de la myPop (symétrique : i parle à j
     * et j parle à i) sous contraintes deux individus qui se rencontrent sont
     * liés au sein du réseau de voisinage
     */
    void rencontreDeuxADeuxConnectesChange() {
        int i = 0;
        int j = 0;
        int bound = sizeP;
        i = myRand.nextInt(0, (sizeP - 1));
        while (myPop[i].tailleVoisinage == 0) { // i a au moins un voisin
            i = myRand.nextInt(0, (sizeP - 1));
        }
        if (myPop[i].opinion[i] == -5.0f) {
            myPop[i].opinion[i] = 0.0f;
        }
        // Controle au the probability to engage a conservation depending on the self-esteem of i
        //if ((float) myRand.nextDouble() < (myPop[i].opinion[i] + 1) / 2.0f) {
        j = ((Individu) (myPop[i].voisins.get((int) (myRand.nextInt(0, (myPop[i].tailleVoisinage - 1)))))).getId();
        if (myPop[i].opinion[j] == -5.0f) {
            myPop[i].opinion[j] = 0.0f;
        }
        // i choisis uniquement des interlocuteurs j qu'il estime proche de lui à delta près
            /*
         * while (Math.abs(myPop[i].opinion[j] - myPop[i].opinion[i]) > delta) {
         * bound--; j = ((Individu) (myPop[i].voisins.get((int)
         * (myRand.nextInt(0, (myPop[i].tailleVoisinage - 1)))))).getId(); if
         * (myPop[i].opinion[j] == -5.0f) { myPop[i].opinion[j] = 0.0f; } if
         * (bound == 0) break ; }
         */
        // i choisis uniquement des interlocuteurs qu'il estime ou qui sont son égal
            /*
         * while (myPop[i].opinion[j] < myPop[i].opinion[i]) { bound--; j =
         * ((Individu) (myPop[i].voisins.get((int) (myRand.nextInt(0,
         * (myPop[i].tailleVoisinage - 1)))))).getId(); if (myPop[i].opinion[j]
         * == -5.0f) { myPop[i].opinion[j] = 0.0f; } if (bound == 0) break ; }
         */
        //System.err.println("i de j "+myPop[i].opinion[j]+" i de i "+myPop[i].opinion[i]+" bound "+bound) ;
        if (bound > 0) { // meaning i has been chosen and i has chosen a convenient j so the encounter can occurs
            //System.err.println("i de j "+myPop[i].opinion[j]+" i de i "+myPop[i].opinion[i]) ;
            Integer ind = new Integer(j);
            if (!myPop[i].acquaintances.contains(ind)) {
                //myPop[i].addAcquaintance(new Integer(j));
                myPop[i].addAcquaintance(ind);
                //myPop[j].knownBy.add(new Integer(i));
            }
            if (myPop[j].opinion[j] == -5.0f) {
                myPop[j].opinion[j] = 0.0f;
            }
            if (myPop[j].opinion[i] == -5.0f) {
                myPop[j].opinion[i] = 0.0f;
            }
            ind = new Integer(i);
            if (!myPop[j].acquaintances.contains(ind)) {
                myPop[j].addAcquaintance(ind);
                //myPop[i].knownBy.add(new Integer(j));
            }
            encounter(i, j);
        }
        //}
    }

    /**
     * J talk to I about the individuals K - gossip LE GOSSIP BASE SUR DIF ENTRE
     * CE QUE I PENSE DE J ET CE QUE I PENSE DE LUI MEME - ESTIME OU MESESTIME
     * RELATIVE DE L'AUTRE
     */
    //public void gossip(int indI, int indJ, int[] indK, float[] difK, double h) {
    public void gossip(int indI, int indJ, int[] indK, double h) {
        if (myPop[indI].ro > 0.0f) {
            for (int i = 0; i < indK.length; i++) {
                //System.err.println(indI + " is gonna hear of " + indK[i]+ "the opinion " + myPop[indJ].opinion[indK[i]] + " and has opinion " + myPop[indI].opinion[indK[i]]);
                //if (myPop[indI].opinion[indK[i]] == -5.0f) {
                if (!myPop[indI].knows(indK[i])) {
                    if (parameters.getMem() > myPop[indI].tailleVoisinage) {
                        //This check is to not cancel from the memory - knownIndiv - an individual's neighbours
                        //if (indI != indK[i]) {
                        if (myPop[indI].acquaintances.size() < parameters.getMem()) {
                            myPop[indI].addAcquaintance(new Integer(indK[i]));
                        } else {
                            //add indK[i] and remove the last
                            // what does that mean an individual can forget its neighbour ?
                            updateKnownIndiv(indI, indK[i], indJ);
                            //System.err.println(indI + " has added " + indK[i] + " removing someone during the gossip");
                        }
                        //}
                    }
                }
                /*
                 * if (myPop[indJ].opinion[indK[i]] < -2.0f) {
                 * System.err.println(myPop[indJ].opinion[indK[i]] + " dit par J
                 * et i part de " + myPop[indI].opinion[indK[i]] + " i " + indI
                 * + " j " + indJ + " k " + indK[i] + " i connait " +
                 * myPop[indI].acquaintances.toString() + " j connait " +
                 * myPop[indJ].acquaintances.toString()); }
                 */
                myPop[indI].opinion[indK[i]] = myPop[indI].opinion[indK[i]] + myPop[indI].ro * (float) h
                        * (myPop[indJ].opinion[indK[i]] + 
                        (-myPop[indJ].boundForImpression + ((float) myRand.nextDouble() * 2 * myPop[indJ].boundForImpression))
                        - myPop[indI].opinion[indK[i]]);
                constraintInBound(indI, indK[i]);
                //System.err.println(indI + " has heard of " + indK[i]+ "the opinion " + myPop[indJ].opinion[indK[i]] + " and has opinion " + myPop[indI].opinion[indK[i]]);
            }
        }
    }

    /**
     * I apply the vanity to J LA VANITE BASE SUR DIF ENTRE CE QUE J PENSE DE I
     * ET CE QUE I PENSE DE LUI MEME - RENVOIE IMAGE POSITIVE OU NEGATIVE DE SOI
     */
    public void vanity(int indI, int indJ, float d) {
        if (parameters.getWe() > 0) {
            myPop[indI].opinion[indJ] = myPop[indI].opinion[indJ] + parameters.getWe() * d;
        }
        constraintInBound(indI, indJ);
        //System.err.println("i "+indI+" j "+indJ+" op i of j "+myPop[indI].opinion[indJ]+" i of i "+myPop[indI].opinion[indI]) ;
    }

    /**
     * I apply the vanity to J LA VANITE BASE SUR DIF ENTRE CE QUE J PENSE DE I
     * ET CE QUE I PENSE DE LUI MEME - RENVOIE IMAGE POSITIVE OU NEGATIVE DE SOI
     * ON MET W = 1 Je le punis d'autant moins que je l'estime et d'autant plus
     * que je ne l'estime pas Je le récompense d'autant moins que je l'estime et
     * d'autant plus que que je ne l'estime pas
     */
    public void vanity(int indI, int indJ, float d, float est) {
        if (parameters.getWe() > 0) {
            //myPop[indI].opinion[indJ] = myPop[indI].opinion[indJ] + w * (1 - est) * d;
            myPop[indI].opinion[indJ] = myPop[indI].opinion[indJ] + parameters.getWe() * est * d;
            //myPop[indI].opinion[indJ] = myPop[indI].opinion[indJ] + w * d;
        }
        constraintInBound(indI, indJ);
        //System.err.println("i "+indI+" j "+indJ+" op i of j "+myPop[indI].opinion[indJ]+" i of i "+myPop[indI].opinion[indI]) ;
    }

    public void constraintInBound(int indI, int indK) {
        //if (Math.abs(myPop[indI].opinion[indK]) > 1.0f && myPop[indI].opinion[indK] != -5.0f) {
        if (Math.abs(myPop[indI].opinion[indK]) > 1.0f && myPop[indI].knows(indK)) {
            //System.err.println(indI+" pense de "+indK+" : "+myPop[indI].opinion[indK]) ;
            if (myPop[indI].opinion[indK] < 0.0f) {
                myPop[indI].opinion[indK] = -1.0f;
            } else {
                myPop[indI].opinion[indK] = 1.0f;
            }
        }
    }

    /*
     * Method to deal with the memory of the individual
     */
    public void updateKnownIndiv(int i, int j, int l) {
        //i is the person we are focusing on; j is the person we are going to add; l is the person we are talking to
        switch (parameters.getHeuristic()) {
            case 1:
                removeAtRandom(i, l);
                break;
            case 2:
                removeLessVivid(i, l);
                break;
            default:
                break;
        }
        myPop[i].addAcquaintance(new Integer(j));
    }

    public void removeOldest(int i, int l) {
        //remove the oldest, neighbours excluded
        int pos = myPop[i].tailleVoisinage - 1;
        //In order to work also for rencontreDeuxADeux
        if (pos == -1) {
            pos = 0;
        }
        if (parameters.getMem() > 1) {
            while (((Integer) myPop[i].acquaintances.get(pos)).intValue() == l) {
                pos++;
            }
        }
        myPop[i].removeAcquaintance(((Integer) myPop[i].acquaintances.get(pos)).intValue(), pos);
        //myPop[((Integer) myPop[i].acquaintances.get(pos)).intValue()].knownBy.remove(myPop[i]);
    }

    public void removeAtRandom(int i, int l) {
        //remove a random individual, neighbours excluded
        int pos = myPop[i].tailleVoisinage - 1;
        int tire;
        //In order to work also for rencontreDeuxADeux
        if (pos == -1) {
            pos = 0;
        }
        tire = (int) (myRand.nextInt(pos, myPop[i].acquaintances.size() - 1));
        if (parameters.getMem() > 1) {
            while (((Integer) myPop[i].acquaintances.get(tire)).intValue() == l) {
                tire = (int) (myRand.nextInt(pos, myPop[i].acquaintances.size() - 1));
            }
        }
        int id = ((Integer) myPop[i].acquaintances.get(tire)).intValue();
        myPop[i].removeAcquaintance(id, tire);
    }

    public void removeLessVivid(int i, int l) {
        //remove a random individual, neighbours excluded
        int pos = myPop[i].tailleVoisinage - 1;
        int tire = 0;
        //In order to work also for rencontreDeuxADeux
        if (pos == -1) {
            pos = 0;
        }
        // choose the less vivid who is not l
        int z = 0;
        float lessvivid = 1.0f;
        int ps = 0;
        for (int r = 0; r < myPop[i].acquaintances.size(); r++) {
            z = myPop[i].acquaintances.get(r).intValue();
            if (Math.abs(myPop[i].opinion[z]) < lessvivid && z != l) {
                tire = z;
                ps = r;
                lessvivid = Math.abs(myPop[i].opinion[z]);
            }
        }
        myPop[i].removeAcquaintance(tire, ps);
    }

    /**
     * demande l'ecriture des r�sultats relatif � connaissance info et adoption
     */
    public void oldEcritResultat(int iter, boolean recupProprieteReseau, int arret, int freqSauvegarde) {
        double tot = 0;
        float mean = 0.0f;
        boolean svg = true;
        double moyenneGlob = 0;
        double minGlob = Double.MAX_VALUE;
        double maxGlob = Double.MIN_VALUE;
        int nbIndEgo = 0; // individus s'aimant et détestant les autres
        int nbIndAbn = 0; // individus ne s'aimant pas et aimant plutôt les autres
        int nbLeader = 0;
        int nbPositive = 0;
        int nbNegative = 0;
        int nbLeaderMax = (int) (sizeP / 2);
        int nbReciprocalLoveLink = 0;
        int nbReciprocalLoveLinkCumul = 0;
        int nbReciprocalLoveLinkIfEgoC = 0;
        int nbReciprocalLoveLinkIfOtherC = 0;
        int nbReciprocalLoveLinkIfLeader = 0;
        float[] loveForFirstLeaders = new float[nbLeaderMax];
        int preferedLeader = -1;
        float bestLoved = 0.0f;
        Arrays.fill(loveForFirstLeaders, 0.0f);
        float ego = 0.0f;
        float moy1 = 0.0f;
        float moy2 = 0.0f;
        int i = 0;
        int j = 0;
        for (i = 0; i < sizeP; i++) {
            moy1 = 0.0f;
            moy2 = 0.0f;
            nbReciprocalLoveLink = 0;
            for (j = 0; j < sizeP; j++) {
                if (myPop[i].opinion[j] > 0.0f) {
                    nbPositive++;
                    if (myPop[j].opinion[i] > 0.0f) {
                        nbReciprocalLoveLinkCumul++;
                        nbReciprocalLoveLink++;
                    }
                } else {
                    nbNegative++;
                }
                if (i == j) {
                    ego = myPop[i].opinion[j]; // amour propre
                } else {
                    moy1 = moy1 + myPop[i].opinion[j]; // taux d'amour de i vis à vis des j 
                    moy2 = moy2 + myPop[j].opinion[i];  // taux d'amour des j vis à vis de i 
                }
                tot = tot + myPop[i].opinion[j];
                if (myPop[i].opinion[j] < minGlob) {
                    minGlob = myPop[i].opinion[j];
                }
                if (myPop[i].opinion[j] > maxGlob) {
                    maxGlob = myPop[i].opinion[j];
                }
            }
            moy1 = moy1 / (float) (sizeP - 1);
            moy2 = moy2 / (float) (sizeP - 1);
            //System.err.println("i "+i+" ego "+ego+" love other "+moy1) ;
            if (ego > moy1 && ego > 0.0f) { // les individus se préfèrent aux autres et s'aiment
                nbIndEgo++;
                nbReciprocalLoveLinkIfEgoC = nbReciprocalLoveLinkIfEgoC + nbReciprocalLoveLink;
            } else { // les individus se déprécient par rapport aux autres et se détestent
                if (ego < moy1 && ego < 0.0f) {
                    nbIndAbn++;
                    nbReciprocalLoveLinkIfOtherC = nbReciprocalLoveLinkIfOtherC + nbReciprocalLoveLink;
                }
            }
            myPop[i].loveForMe = moy2;

            // l'influence de J sur moi est lié à combien je le trouve mieux que moi ou le contraire et à combien il est sûr de lui (delta IJ est la certitude de J percu par I)
            if (moy2 > 0) { // if it is liked, it is a leader
                if (!myPop[i].leader) {
                    myPop[i].leader = true;
                    myPop[i].timeLeadership = 1;
                } else {
                    myPop[i].timeLeadership++;
                }
                nbLeader++;
                nbReciprocalLoveLinkIfLeader = nbReciprocalLoveLinkIfLeader + nbReciprocalLoveLink;
                if (nbLeader < nbLeaderMax && nbLeader > 0) {
                    loveForFirstLeaders[nbLeader - 1] = myPop[i].loveForMe;
                }
                if (bestLoved < myPop[i].loveForMe && nbLeader > 0) {
                    bestLoved = myPop[i].loveForMe;
                    preferedLeader = i;
                }
            } else {
                if (myPop[i].leader) {
                    myPop[i].leader = false;
                    myPop[i].timeLeadership = 0;
                }
            }
        }
        Arrays.sort(loveForFirstLeaders);
        if (iter > 0) {
            svg = ((iter) % freqSauvegarde == 0);
        }
        if (svg) {
            printParametres();
            moyenneGlob = (float) (tot / (sizeP * sizeP));
            if (moyenneGlob > 0) {
                nbLeader = 0;
            }
            System.out.print(iter + ";");
            System.out.print(((new Double(moyenneGlob)).toString()).replace(".", ",") + ";");
            System.out.print(((new Double(minGlob)).toString()).replace(".", ",") + ";");
            System.out.print(((new Double(maxGlob)).toString()).replace(".", ",") + ";");
            System.out.print(nbPositive + ";");
            System.out.print(nbNegative + ";");
            System.out.print(((new Double((float) nbReciprocalLoveLinkCumul / (float) sizeP)).toString()).replace(".", ",") + ";");
            if (nbIndEgo > 0) {
                System.out.print(((new Double((float) nbReciprocalLoveLinkIfEgoC / (float) nbIndEgo)).toString()).replace(".", ",") + ";");
            } else {
                System.out.print(0 + ";");
            }
            if (nbIndAbn > 0) {
                System.out.print(((new Double((float) nbReciprocalLoveLinkIfOtherC / (float) nbIndAbn)).toString()).replace(".", ",") + ";");
            } else {
                System.out.print(0 + ";");
            }
            if (nbLeader > 0) {
                System.out.print(((new Double((float) nbReciprocalLoveLinkIfLeader / (float) nbLeader)).toString()).replace(".", ",") + ";");
            } else {
                System.out.print(0 + ";");
            }
            System.out.print(nbIndEgo + ";");
            System.out.print(nbIndAbn + ";");
            System.out.print(nbLeader + ";");
            int tMax = 0;
            int leader = 0; // oldest leader
            double loveLeader = 0.0; // love for the oldest leader
            for (j = 0; j < sizeP; j++) {
                if (myPop[j].timeLeadership > tMax) {
                    leader = j;
                    tMax = myPop[j].timeLeadership;
                }
            }
            for (j = 0; j < sizeP; j++) {
                loveLeader = loveLeader + myPop[j].opinion[leader];
            }
            loveLeader = loveLeader / (double) sizeP;
            System.out.print(tMax + ";");
            if (preferedLeader > -1) {
                System.out.print(myPop[preferedLeader].timeLeadership + ";");
            } else {
                System.out.print(0 + ";");
            }
            System.out.print(((new Double(loveLeader)).toString()).replace(".", ",") + ";");
            for (j = nbLeaderMax - 1; j >= 0; j--) {
                System.out.print(((new Double(loveForFirstLeaders[j])).toString()).replace(".", ",") + ";");
            }
            //selfEsteemBiaisWrite();
            //selfEsteemBiaisWrite2();
            System.out.println((new Float(getDominance()).toString()).replace(".", ",") + ";");
            //System.out.println();
            //System.err.println(moyenneGlob + " " + minGlob + " " + maxGlob + " " + nbIndEgo + " " + nbIndAbn + " " + nbLeader + " " + tMax + " " + loveLeader+" "+nbPositive+" "+nbNegative);
        }

    }

    public void writingPatternDiagnosis(long iter) {
        if (patternDiagnosis) {
            //printParametres();
            mod.ps1.print(iter + ";");
            mod.ps1.print(patternDiagnosis() + ";");
        }
    }

    public void writingPatternSynthesis(long iter) {
        printListParametres();
        mod.ps2.print(iter + ";");
        mod.ps2.print(nbDominance + ";");
        mod.ps2.print(nbHierarchy + ";");
        mod.ps2.print(nbSmallWorld + ";");
        mod.ps2.print(nbElite + ";");
        mod.ps2.print(nbCrisis + ";");
        mod.ps2.println();
    }

    public void writingOutputFile(long iter, boolean writeMatrix, boolean writeDurationEpisode, boolean writeLeader, boolean writeHisto, boolean writeAval, boolean writeAtSave) {
        int i = 0;
        int j = 0;
        /*
         * if (writeMatrix) { mod.ps1.println(); mod.ps1.println("iter" + ";" +
         * iter); for (i = 0; i < sizeP; i++) { for (j = 0; j < sizeP; j++) {
         * mod.ps1.print((new
         * Float(myPop[i].opinion[j]).toString()).replace(".", ",") + ";"); }
         * mod.ps1.println(); } }
         */
        //printParametres();
        //mod.ps2.print(iter + ";");
        /*
         * For Sylvie followLeaders(); double[] influence =
         * averageInfluencePower(); int s = sizeP - 1; for (i = s; i >= 0; i--)
         * { mod.ps2.print((new Float(influence[i]).toString()).replace(".",
         * ",") + ";"); }
         *
         */

        if (writeAtSave) {
            printParametres();
            mod.ps2.print(iter + ";");
            mod.ps2.print("wDominance" + ";");
            mod.ps2.print((new Float(getDominance()).toString()).replace(".", ",") + ";");
            mod.ps2.print((new Float(averageOpinion).toString()).replace(".", ",") + ";");
            mod.ps2.print((new Float(leader).toString()).replace(".", ",") + ";");
            mod.ps2.print((new Float(maxReputation).toString()).replace(".", ",") + ";");
            mod.ps2.print((new Float(secondMaxReputation).toString()).replace(".", ",") + ";");
            mod.ps2.println((new Float(minReputation).toString()).replace(".", ",") + ";");
            mod.ps2.print((new Float(leader).toString()).replace(".", ",") + ";");
            //mod.ps2.print((new Float(v).toString()).replace(".", ",") + ";");
            //mod.ps2.print((new Float(maxReputation).toString()).replace(".", ",") + ";");
            //mod.ps2.print((new Float(myPop[secondLeader].opinion[leader]).toString()).replace(".", ",") + ";");
        }
        computeStatistics();
        mod.ps2.print(nbSelfLovers + ";");
        mod.ps2.print(nbLovedByAll + ";");
        mod.ps2.print(nbHatedByAll + ";");

        if (writeDurationEpisode) {
            printParametres();
            mod.ps2.print(iter + ";");
            mod.ps2.print("wLength" + ";");
            mod.ps2.print((new Float(getDominance()).toString()).replace(".", ",") + ";");
            mod.ps2.print((new Float(averageOpinion).toString()).replace(".", ",") + ";");
            mod.ps2.print((new Float(maxReputation).toString()).replace(".", ",") + ";");
            mod.ps2.print((new Float(secondMaxReputation).toString()).replace(".", ",") + ";");
            mod.ps2.print((new Float(minReputation).toString()).replace(".", ",") + ";");
            //mod.ps2.print((new Float(v).toString()).replace(".", ",") + ";");
            //mod.ps2.print((new Float(maxReputation).toString()).replace(".", ",") + ";");
            //mod.ps2.print((new Float(myPop[secondLeader].opinion[leader]).toString()).replace(".", ",") + ";");
        }

        if (patternDiagnosis) {
            printParametres();
            mod.ps2.print(iter + ";");
            mod.ps2.print((new Integer(patternDiagnosis()).toString()).replace(".", ",") + ";");
        }
        writeHisto = false;
        if (writeHisto) {
            //Write in one column the classes and in the other the frequencies mod.ps4.println(); 
            mod.ps4.println("iter" + ";" + iter);
            float[] result = getDistribKnownBy();
            int pos = 0;
            for (int k = 0; k < result.length; k++) {
                pos = (int) ((k + 1) * widthClasses * 100);
                mod.ps4.println(pos + ";" + (new Float(result[k]).toString()).replace(".", ",") + ";");
            }
        }
    }

    // Writing the results in order to create a database of the results of an experiment
    public void writeForDataBase(long time) {
        int i = 0;
        int j = 0;
        for (i = 0; i < sizeP; i++) {
            mod.ps1.print(expeLotName);
            mod.ps1.print(";" + parameters.getSimu()); // num of the expe lot (simu)
            mod.ps1.print(";" + parameters.getRep()); // name of the expe (rep)
            mod.ps1.print(";" + time); // iter
            mod.ps1.print(";" + i); // num of indiv i
            for (j = 0; j < sizeP; j++) {
                mod.ps1.print(";" + (new Float(myPop[i].opinion[j]).toString()).replace(".", ",")); // opinion vector of indiv i
            }
            mod.ps1.println();
        }
    }

    /**
     * demande l'ecriture des r�sultats relatif � connaissance info et adoption
     */
    public void editResultat() {
        for (int z = 0; z < sizeP; z++) {
            for (int tu = 0; tu < sizeP; tu++) {
                System.err.print(myPop[z].opinion[tu] + " ");
            }
        }
        System.err.println();
    }

    public void printParametres() {
        printParametres(mod.ps2);
    }

    public void printParametres(PrintStream ps) {
        ps.print(parameters.getValues(";"));
    }

    public void printListParametres() {
        mod.ps2.print(expeLotName + ";");
        printParametres();
    }

    public void printParametersForPs1_3_4_5() {
        printParametres(mod.ps1);
        mod.ps1.println();
        mod.ps1.println();
        printParametres(mod.ps3);
        mod.ps3.println();
        mod.ps3.println();
        mod.ps3.println("Leadership" + ";");
        printParametres(mod.ps4);
        mod.ps4.println();
        mod.ps4.println();
        mod.ps4.println("% of popularity" + ";" + "% of people");
        printParametres(mod.ps5);
        mod.ps5.println();
        mod.ps5.println();
        mod.ps5.print("Avalanche" + ";");
        mod.ps5.println("Type" + ";");
        //mod.ps5.println("2's reputation" + ";");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CODES FOR NETWORK //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Initialisation du réseau de voisinage (des individus) - si 0,
     * connectivité complète ; si 1 = rencontre deux à deux (pas de réseau,
     * initialisation faite dans initmyPop() ; si 2 = réseau constant ; si 3 =
     * réseau aléatoire
     */
    //public ArrayList[] initReseau(int typeRes, int moy, int seed, float bruitage) {
    public ArrayList[] initReseau(int typeRes, int moy, float noise, int seed) {
        ArrayList reseau[] = new ArrayList[sizeP];
        switch (typeRes) {
            case 0: // complete connectivity
                int tailleVoisinage = sizeP - 1;
                for (int i = 0; i < sizeP; i++) {
                    reseau[i] = new ArrayList();
                    int j = 0;
                    for (j = 0; j < i; j++) {
                        reseau[i].add(myPop[j]);
                    }
                    int z = i + 1;
                    for (j = z; j < sizeP; j++) {
                        reseau[i].add(myPop[j]);
                    }
                }
                break;
            case 1: // réseau de groupe
                int nbGroupes = moy;
                double pLinkToOthers = noise;
                //attribution d'un groupe parmi les nbgroupes groupes possibles
                // pour chaque i, lui donner pour réseau son groupe
                int tailleVoisinag = (int) ((double) sizeP / (double) nbGroupes) - 1; // = taille d'un groupe-1
                if (tailleVoisinag * nbGroupes < (sizeP - (1 * nbGroupes))) {
                    int sizeP2 = (tailleVoisinag + 1) * nbGroupes; // redimensionnement sizeP pour assurer uniformité initiale taille des groupes
                    Individu[] myPop2 = new Individu[sizeP2];
                    for (int j = 0; j < sizeP2; j++) {
                        myPop2[j] = myPop[j];
                    }
                    myPop = myPop2;
                    sizeP = myPop.length;
                }
                //System.err.println("size Pop " + sizeP + " taille des groupes " + (tailleVoisinag + 1) + " nb groupes " + nbGroupes);
                for (int g = 1; g <= nbGroupes; g++) {
                    for (int i = (tailleVoisinag * (g - 1)); i <= (tailleVoisinag * g); i++) {
                        myPop[i].group = g;
                    }
                }
                int groupRef = -1;
                for (int i = 0; i < sizeP; i++) {
                    reseau[i] = new ArrayList();
                }
                for (int i = 0; i < sizeP; i++) {
                    groupRef = myPop[i].group;
                    for (int j = 0; j < sizeP; j++) {
                        if (j != i) {
                            if (myPop[j].group == groupRef) {
                                reseau[i].add(myPop[j]);
                            } else { // not a group member of i
                                if (Math.random() < pLinkToOthers) {
                                    reseau[i].add(myPop[j]);
                                    // to ensure the network is symetric
                                    if (!reseau[j].contains(myPop[i])) {
                                        reseau[j].add(myPop[i]); // to ensure the network is symetric
                                    }
                                }
                            }
                        }
                    }
                }
                // ajouter membre autre groupe à son réseau avec proba p d'être lié à autres groupes
                break;
            case 2: // constant number of neighbours size moy
                reseau = reseauConstant(moy, seed);
                break;
            case 3: // random network with on averageReputation moy neighbours
                reseau = reseauAleatoire(moy, seed);
                break;
            case 4: // scale free
                reseau = reseauScaleFree(moy, seed);
                break;
            case 5: // small world with no dimension base 
                //reseau = reseauRegulierBruite(moy, bruitage, 1, seed);
                reseau = reseauRegulierBruite(moy, noise, 1, seed);
                break;
            case 6: // small world network based on Moore's lattice
                //reseau = reseauRegulierBruite(moy, bruitage, 2, seed);
                reseau = reseauRegulierBruite(8, noise, 2, seed);
                break;
            case 7: // small world network based on vonNeumann's lattice
                //reseau = reseauRegulierBruite(moy, bruitage, 3, seed);
                reseau = reseauRegulierBruite(4, noise, 3, seed);
            case 8: // réseau de groupe unique et individus extérieurs isolés dont on peut parler mais qu'on ne peut pas rencontrer
                //int sizeP2 = (int)((float)sizeP / 2.0f); // redimensionnement sizeP pour assurer uniformité initiale taille des groupes
                int groupSize = 35;
                int sizeP2 = groupSize;
                for (int i = 0; i < sizeP; i++) {
                    reseau[i] = new ArrayList();
                }
                for (int i = 0; i < sizeP2; i++) {
                    myPop[i].group = 0;
                    for (int j = 0; j < sizeP2; j++) {
                        if (i != j) {
                            reseau[i].add(myPop[j]);
                        }
                    }
                    for (int j = sizeP2; j < sizeP; j++) {
                        myPop[i].addAcquaintance(j);
                    }
                }
                for (int i = sizeP2; i < sizeP; i++) {
                    myPop[i].group = 1;
                }
                // ajouter membre autre groupe à son réseau avec proba p d'être lié à autres groupes
                break;
            case 11: // lattice Von Neumann avec visualisation
                if (moy == 4) {
                    reseau = latticeVonNeumann();
                }
                if (moy == 8) {
                    reseau = latticeMoore();
                }
                //setVisu(true);
                break;
            case 12: // lattice Von Neumann sans visualisation
                if (moy == 4) {
                    reseau = latticeVonNeumann();
                }
                if (moy == 8) {
                    reseau = latticeMoore();
                }
                break;
            default:
                break;
        }
        return reseau;
    }

    /**
     * Construction d'un réseau "small world" basé sur un réseau régulier qu'on
     * a bruité selon une probabilité beta (déplacement aléatoire de liens)
     * encore appelé modèle Beta pour faire croître C (le coefficient de
     * clustering)
     */
//private ArrayList[] reseauRegulierBruite(int nVoisins, float bruitage, int regulierBase, int graineAleat) {
    private ArrayList[] reseauRegulierBruite(int nVoisins, float noise, int regulierBase, int graineAleat) {
        ArrayList reseau[] = new ArrayList[sizeP];
        switch (regulierBase) {
            case 1:
                reseau = reseauConstant(nVoisins, graineAleat);
                break;
            case 2:
                reseau = latticeMoore();
                break;
            case 3:
                reseau = latticeVonNeumann();
                break;
            default:
                System.err.println("Je ne sais pas sur quel réseau régulier de base construire mon small world");
                System.exit(0);
                break;
        }
        // Bruitage du réseau régulier de base avec probabilité "bruitage" =
        // beta
        //Uniform v = new Uniform(0, sizeP - 1, graineAleat);
        ArrayList reseauBruite[] = new ArrayList[sizeP];
        int i, j = 0;
        for (i = 0; i < sizeP; i++) {
            reseauBruite[i] = new ArrayList();
            for (j = 0; j < nVoisins; j++) {
                Individu ind = ((Individu) (reseau[i].get(j)));
                reseauBruite[i].add(ind);
            }
        }

        // POUR REGLER LA SUPPRESSION DE BRUITAGE
        float bruitage = noise;

        // FIN REGLAGE PAS PROPRE
        if (bruitage > 0f) {
            int vois, k = 0;
            boolean dejaVois = false;
            for (i = 0; i < sizeP; i++) {
                for (j = 0; j < nVoisins; j++) {
                    if (reseau[i].size() < nVoisins) {
                        System.err.println(" i " + i + " j " + j + " ss "
                                + reseau[i].size());
                        System.err.println(reseau[i]);
                        System.exit(0);
                    }
                    // on tire un nb aléatoire et on compare à Beta pour
                    // voir si on déplace le lien
                    float nbAleat = (float) myRand.nextDouble();
                    if (nbAleat < bruitage) { // pour l'individu i, on met
                        // un autre voisin j
                        int in = ((Individu) (reseau[i].get(j))).getId();
                        for (int n = 0; n < reseauBruite[i].size(); n++) {
                            if (((Individu) reseauBruite[i].get(n)).getId() == in) {
                                reseauBruite[i].remove(n);
                            }
                        }
                        for (int n = 0; n < reseauBruite[in].size(); n++) {
                            if (((Individu) reseauBruite[in].get(n)).getId() == i) {
                                reseauBruite[in].remove(n);
                            }
                        }
                        vois = myRand.nextInt(0, sizeP - 1);
                        dejaVois = true;
                        while (vois == i) { // vois doit être différent de i
                            vois = myRand.nextInt(0, sizeP - 1);
                        }
                        // vois ne doit pas être déjà vois de i et différent de
                        // i
                        while (dejaVois) {
                            dejaVois = false;
                            // Individu ind = (Individu)myPop[i] ;
                            for (k = 0; k < reseauBruite[i].size(); k++) {

                                if (((Individu) (reseauBruite[i].get(k))) == myPop[vois]) {
                                    dejaVois = true;
                                    k = reseauBruite[i].size();
                                    vois = myRand.nextInt(0, sizeP - 1);
                                    while (vois == i) { // vois doit être
                                        // différent de i
                                        vois = myRand.nextInt(0, sizeP - 1);
                                    }
                                }
                            }
                        }
                        reseauBruite[i].add(myPop[vois]);
                        reseauBruite[vois].add(myPop[i]);
                    }
                }
            }
            reseau = null;
        }
        return reseauBruite;
    }

    /**
     * Initialisation du réseau de voisinage constant (tous individus ont même
     * nombre de voisins)
     */
    private ArrayList[] reseauConstant(int nVoisins, int graineAleat) {
        //myRand = new Random();
        ArrayList reseau[] = new ArrayList[sizeP];
        for (int i = 0; i < sizeP; i++) {
            reseau[i] = new ArrayList();
        }
        int i, alea1, alea2, nvois, numVois, nv;
        int liensSup;
        int cpt = 0;
        int lim = Math.min(sizeP - 1, nVoisins);
        int nbLiensMax = Math.min(lim, sizeP - 1) * sizeP / 2;
        int nbLiens = nbLiensMax;
        int al1, al2, indi;
        int taille = sizeP;
        int[] indices = new int[taille];
        int nPasPourvus = taille; // nb d'individus entièrement pourvus en
        // liens : à la fin
        for (i = 0; i < indices.length; i++) {
            indices[i] = i;
        }
        boolean voisin = false;
        while (nbLiens > 0) {
            voisin = false;
            al1 = (int) (myRand.nextDouble() * (double) (nPasPourvus));
            al2 = (int) (myRand.nextDouble() * (double) (nPasPourvus));
            cpt++;
            alea1 = indices[al1];
            alea2 = indices[al2];
            voisin = voisins(reseau[alea1], alea2);
            if (alea1 != alea2 && !voisin && reseau[alea1].size() < lim
                    && reseau[alea2].size() < lim) {
                // les deux individus font connaissance
                reseau[alea1].add(myPop[alea2]);
                reseau[alea2].add(myPop[alea1]);
                nbLiens--; // un lien de moins à trouver
                cpt = 0; // on remet le cpt à 0
                // on vérifie si l'un des deux a tous ses liens pourvus
                // si oui, on met son indice en fin de tableau, et le nb d'indiv
                // encore à pourvoir diminue
                if (reseau[alea1].size() == lim) {
                    indices[al1] = indices[nPasPourvus - 1];
                    indices[nPasPourvus - 1] = alea1;
                    if (indices[al1] == alea2) {
                        al2 = al1;
                    }
                    nPasPourvus--;
                }
                if (reseau[alea2].size() == lim) {
                    indices[al2] = indices[nPasPourvus - 1];
                    indices[nPasPourvus - 1] = alea2;
                    nPasPourvus--;
                }
            } else if (cpt > nPasPourvus * nPasPourvus
                    && nbLiens < nbLiensMax / 2) {
                // on supprime quelques liens
                liensSup = 0;
                indi = -1;
                int num2 = 0;
                while (liensSup < 1) {
                    al1 = nPasPourvus
                            - 1
                            + (int) (myRand.nextDouble() * (double) (taille - nPasPourvus));
                    alea1 = indices[al1];
                    if (reseau[alea1].size() > 0) {
                        alea2 = (int) (myRand.nextDouble() * (double) (reseau[alea1].size() - 1));
                        // Enlever le voisin alea2 à l'individu alea1 et
                        // vice-versa
                        int tail = reseau[alea1].size();
                        for (int l = 0; l < tail; l++) {
                            if (l == alea2) {
                                num2 = ((Individu) reseau[alea1].get(l)).getId();
                                reseau[alea1].remove(l);
                                l = tail;
                            }
                        }
                        tail = reseau[num2].size();
                        for (int l = 0; l < tail; l++) {
                            if (((Individu) reseau[num2].get(l)).getId() == alea1) {
                                reseau[num2].remove(l);
                                l = tail;
                            }
                        }
                        liensSup++;
                        // l'indiv1 a au moins un lien à pourvoir
                        if (nPasPourvus < taille
                                && reseau[alea1].size() == lim - 1) {
                            indices[al1] = indices[nPasPourvus];
                            indices[nPasPourvus] = alea1;
                            nPasPourvus = Math.min(nPasPourvus + 1, taille);
                        }
                        indi = -1;
                        // on le cherche dans le tableau des indices
                        for (int k = 0; k < taille; k++) {
                            if (myPop[indices[k]].id == num2) {
                                indi = k;
                                alea2 = indices[indi];
                            }
                        }
                        // on vérifie si le statut de indiv2 a changé
                        if (nPasPourvus < taille
                                && reseau[alea2].size() == lim - 1) {
                            if (indi != -1) {
                                indices[indi] = indices[nPasPourvus];
                                indices[nPasPourvus] = alea2;
                                nPasPourvus = Math.min(nPasPourvus + 1, taille);
                            }
                        }
                    }
                }
                nbLiens = Math.min(nbLiensMax, nbLiens + liensSup);
                cpt = 0;
            }
        }
        for (i = 0; i < taille; i++) {
            if (reseau[i].size() != lim) {
                System.err.println("###### attention taille voisinage !");
            }
        }
        return reseau;
    }

    /**
     * Méthode permettant de déterminer si deux individus sont voisins
     */
    private boolean voisins(ArrayList voisinage, int numVoisin) {
        boolean vois = false;
        int longVois = voisinage.size();
        if (longVois == 0) {
            vois = false;
        } else {
            for (int i = 0; i < longVois; i++) { // vérification qu'ils ne
                // sont pas déjà voisins
                if (((Individu) voisinage.get(i)).getId() == numVoisin) {
                    vois = true;
                    i = longVois;
                }
            }
        }
        return vois;
    }

    /**
     * Génération d'un réseau aléatoire
     */
    private ArrayList[] reseauAleatoire(int nVoisinsMoyen, int graineAleat) {
        //myRand = new Random();
        ArrayList reseau[] = new ArrayList[sizeP];
        for (int i = 0; i < sizeP; i++) {
            reseau[i] = new ArrayList();
        }
        // nombre de liens à réaliser
        int nLienR = (int) ((sizeP
                * Math.min(sizeP - 1, nVoisinsMoyen) + 1) / 2);
        int alea1, alea2;
        //Uniform v = new Uniform(0, sizeP - 1, graineAleat);
        int i = 0;
        boolean voisin = false;
        while (nLienR > 0) {
            alea1 = myRand.nextInt(0, sizeP - 1);
            alea2 = myRand.nextInt(0, sizeP - 1);
            if (alea1 != alea2) {
                int longVois = reseau[alea1].size();
                if (longVois == 0) {
                    voisin = false;
                }
                for (i = 0; i < longVois; i++) { // vérification qu'ils ne
                    // sont pas déjà voisins
                    if (((Individu) reseau[alea1].get(i)).equals(myPop[alea2])) {
                        voisin = true;
                    }
                }
                if (!voisin) {
                    // les deux individus font connaissance
                    reseau[alea1].add(myPop[alea2]);
                    reseau[alea2].add(myPop[alea1]);
                    // un lien de moins à trouver
                    nLienR--;
                } else {
                    voisin = false;
                }
            }
        }
        return reseau;
    }

    /**
     * Génération d'un réseau sur grille, voisinage de VonNeumann (4 voisins à
     * l'Ouest, Est, au Nord et au Sud de l'individu considéré)
     */
    private ArrayList[] latticeVonNeumann() {
        ArrayList reseau[] = new ArrayList[sizeP];
        int width = (int) Math.sqrt(sizeP);
        if ((width * width) != sizeP) {
            System.err.println("Attention, la taille de la myPop ne renvoie pas une racine carrée entière "
                    + "et il y a des problèmes pour construire la grille de voisinage");
            System.out.println("Attention, la taille de la myPop ne renvoie pas une racine carrée entière "
                    + "et il y a des problèmes pour construire la grille de voisinage");
        }
        int i = 0;
        for (i = 0; i < sizeP; i++) {
            reseau[i] = new ArrayList();
            if (i < width) {
                if (i == 0) {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[width - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i + sizeP - width]);
                } else if (i == width - 1) {
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i + sizeP - width]);
                } else {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i + sizeP - width]);
                }
            } else if (i >= (sizeP - width) && i < sizeP) {
                if (i == (sizeP - width)) {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i + width - 1]);
                    reseau[i].add(myPop[i - sizeP + width]);
                    reseau[i].add(myPop[i - width]);
                } else if (i == sizeP - 1) {
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i - sizeP + width]);
                    reseau[i].add(myPop[i - width]);
                } else {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i - sizeP + width]);
                    reseau[i].add(myPop[i - width]);
                }
            } else {
                if (i % width == 0) {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i + width - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i - width]);
                } else if ((i + 1) % width == 0) {
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i - width]);
                } else {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i - width]);
                }
            }
            if (reseau[i].size() < 4) {
                System.err.println("Erreur taille pour i " + i + " : "
                        + reseau[i]);
                System.exit(0);
            }
        }
        return reseau;
    }

    /**
     * Génération d'un réseau sur grille, voisinage de Moore (8 voisins) (4
     * voisins à l'Ouest, Est, au Nord et au Sud de l'individu considéré et 4
     * voisins NO SO SE et NE)
     */
    private ArrayList[] latticeMoore() {
        ArrayList reseau[] = new ArrayList[sizeP];
        int width = (int) Math.sqrt(sizeP);
        if ((width * width) != sizeP) {
            System.err.println("Attention, la taille de la myPop ne renvoie pas une racine carrée entière "
                    + "et il y a des problèmes pour construire la grille de voisinage");
            System.out.println("Attention, la taille de la myPop ne renvoie pas une racine carrée entière "
                    + "et il y a des problèmes pour construire la grille de voisinage");
        }
        int i = 0;
        for (i = 0; i < sizeP; i++) {
            reseau[i] = new ArrayList();
            if (i < width) {
                if (i == 0) {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[width - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i + sizeP - width]);
                    reseau[i].add(myPop[i + sizeP - width + 1]);
                    reseau[i].add(myPop[i + sizeP - 1]);
                    reseau[i].add(myPop[i + width + 1]);
                    reseau[i].add(myPop[i + (2 * width) - 1]);

                } else if (i == width - 1) {
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i + sizeP - width]);
                    reseau[i].add(myPop[i + width - 1]);
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i + sizeP - width - 1]);
                    reseau[i].add(myPop[sizeP - width]);
                } else {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i + width - 1]);
                    reseau[i].add(myPop[i + width + 1]);
                    reseau[i].add(myPop[i + sizeP - width]);
                    reseau[i].add(myPop[i + sizeP - width - 1]);
                    reseau[i].add(myPop[i + sizeP - width + 1]);
                }
            } else if (i >= (sizeP - width) && i < sizeP) {
                if (i == (sizeP - width)) {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i + width - 1]);
                    reseau[i].add(myPop[i - sizeP + width]);
                    reseau[i].add(myPop[i - width]);
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i - sizeP + width + 1]);
                    reseau[i].add(myPop[i - sizeP + (2 * width)
                            - 1]);
                } else if (i == sizeP - 1) {
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i - sizeP + width]);
                    reseau[i].add(myPop[i - sizeP + width - 1]);
                    reseau[i].add(myPop[i - sizeP + 1]);
                    reseau[i].add(myPop[i - width]);
                    reseau[i].add(myPop[i - width - 1]);
                    reseau[i].add(myPop[i - (2 * width) + 1]);
                } else {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i - sizeP + width]);
                    reseau[i].add(myPop[i - width]);
                    reseau[i].add(myPop[i - width - 1]);
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - sizeP + width - 1]);
                    reseau[i].add(myPop[i - sizeP + width + 1]);
                }
            } else {
                if (i % width == 0) {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i + width - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i + width + 1]);
                    reseau[i].add(myPop[i + (2 * width) - 1]);
                    reseau[i].add(myPop[i - width]);
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - 1]);
                } else if ((i + 1) % width == 0) {
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i + width - 1]);
                    reseau[i].add(myPop[i - width]);
                    reseau[i].add(myPop[i - width - 1]);
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i - (2 * width) + 1]);
                } else {
                    reseau[i].add(myPop[i + 1]);
                    reseau[i].add(myPop[i - 1]);
                    reseau[i].add(myPop[i + width]);
                    reseau[i].add(myPop[i + width + 1]);
                    reseau[i].add(myPop[i + width - 1]);
                    reseau[i].add(myPop[i - width]);
                    reseau[i].add(myPop[i - width + 1]);
                    reseau[i].add(myPop[i - width - 1]);
                }
            }
            // System.err.println("i "+i+" : "+reseau[i]) ;
        }
        return reseau;
    }

    /**
     * Génération d'un réseau scale-free suivant une loi de puissance (réseaux
     * sans échelle) RETENIR TIRAGE FAIT A PARTIR CUMUL DES CONNECTIVITES
     * DIRECTEMENT POUR SIMPLIFIER (= tirage selon proba) // Algo "Attachement
     * Préférentiel"
     */
    private ArrayList[] reseauScaleFree(int m, int graineAleat) {
        //Uniform v = new Uniform(0, sizeP - 1, graineAleat);
        ArrayList reseau[] = new ArrayList[sizeP];
        for (int i = 0; i < sizeP; i++) {
            reseau[i] = new ArrayList();
        }
        double[] proba = new double[sizeP];
        boolean[] individuPris = new boolean[sizeP];
        int[] connectivite = new int[sizeP];
        int[] connectiviteCumul = new int[sizeP];
        java.util.Arrays.fill(connectivite, 0);
        java.util.Arrays.fill(connectiviteCumul, 0);
        java.util.Arrays.fill(proba, 0.0);
        java.util.Arrays.fill(individuPris, false);
        int i = 0;
        int ind = 0;
        int individusPlaces = 0;
        // PREMIERE ITERATION
        // On choisit m individus different et on les lie à un nouvel individu
        int nouvelInd = myRand.nextInt(0, sizeP - 1);
        individuPris[nouvelInd] = true;
        for (i = 0; i < m; i++) {
            ind = myRand.nextInt(0, sizeP - 1);
            while (individuPris[ind]) {
                ind = myRand.nextInt(0, sizeP - 1);
            }
            individuPris[ind] = true;
            // Les individus sont liés
            reseau[ind].add(myPop[nouvelInd]);
            reseau[nouvelInd].add(myPop[ind]);
            // On modifie la connectivité
            connectivite[ind] = connectivite[ind] + 1;
            connectivite[nouvelInd] = connectivite[nouvelInd] + 1;
        }
        // On calcule les connectivites cumulées
        int temp = 0;
        for (i = 0; i < sizeP; i++) {
            connectiviteCumul[i] = connectivite[i] + temp;
            temp = connectiviteCumul[i];
        }
        individusPlaces = m + 1;
        // ITERATIONS SUIVANTES : introduction de l'attachement préférentiel
        while (individusPlaces != sizeP) {
            int compteur = 0;
            //int b = 0;
            int c = 0;
            int d = 0;
            boolean nonTrouve = true;
            int[] ajouteA = new int[m];
            nouvelInd = myRand.nextInt(0, sizeP - 1);
            while (individuPris[nouvelInd]) {
                nouvelInd = myRand.nextInt(0, sizeP - 1);
            }
            while (compteur != m) {
                nonTrouve = true;
                while (nonTrouve) {
                    c = (int) (myRand.nextDouble() * connectiviteCumul[sizeP - 1]);
                    for (d = 0; d < sizeP; d++) {
                        if (c < connectiviteCumul[d]) {
                            ind = d;
                            d = sizeP;
                        }
                    }
                    nonTrouve = false;
                    for (int g = 0; g < compteur; g++) {
                        if (ind == ajouteA[g]) {
                            nonTrouve = true;
                            g = compteur;
                        }
                    }
                }
                ajouteA[compteur] = ind;
                // On tire une proba, si valeur tirée < proba de l'individu
                // alors on lie
                reseau[ind].add(myPop[nouvelInd]);
                reseau[nouvelInd].add(myPop[ind]);
                // On modifie la connectivité
                connectivite[ind] = connectivite[ind] + 1;
                connectivite[nouvelInd] = connectivite[nouvelInd] + 1;
                compteur++;
            }
            individuPris[nouvelInd] = true;
            // On calcule les connectivites cumulées
            temp = 0;
            for (i = 0; i < sizeP; i++) {
                connectiviteCumul[i] = connectivite[i] + temp;
                temp = connectiviteCumul[i];
            }
            individusPlaces++;
        }
        return reseau;
    }

    /**
     * Calcul et édition des propriétés initiales du réseau
     */
    public void calculAndEditProperties() {
        ///*/ Suppression temporaire du calcul de path length L

        // CALCUL DE SHORTER PATH LENGTH DISTRIBUTION*******************************
        int[][] distanceCouples = new int[sizeP][sizeP];
        int i, j, z = 0;
        for (i = 0; i < sizeP; i++) {
            Arrays.fill(distanceCouples[i], 0);
        }
        int dist = 0;
        int distMax = 0;
        int ind = 0;
        int vois = 0;
        boolean pasFini = true;
        boolean nextI = false;
        int nbDejaVu = 0;
        ArrayList voisinARegarderEnCours = new ArrayList();
        ArrayList voisinARegarderSuivant = new ArrayList();
        boolean[] dejaVu = new boolean[sizeP];
        for (i = 0; i < sizeP; i++) {
            nextI = true;
            // On ne fait pas le calcul si tous les j sauf j = i ont une
            // distanceCouple différentes de 0
            for (j = 0; j < i; j++) {
                if (distanceCouples[i][j] == 0) {
                    nextI = false;
                }
            }
            for (j = i + 1; j < sizeP; j++) {
                if (distanceCouples[i][j] == 0) {
                    nextI = false;
                }
            }
            if (!nextI) {
                Arrays.fill(dejaVu, false);
                dejaVu[i] = true;
                nbDejaVu++;
                voisinARegarderEnCours.add(new Integer(i));
                dist = 1;
                pasFini = true;
                while (pasFini) {
                    for (j = 0; j < voisinARegarderEnCours.size(); j++) {
                        ind = ((Integer) (voisinARegarderEnCours.get(j))).intValue();
                        for (z = 0; z < myPop[ind].tailleVoisinage; z++) {
                            vois = ((Individu) (myPop[ind].voisins.get(z))).getId();
                            if (dejaVu[vois] == false) {
                                distanceCouples[i][vois] = dist;
                                voisinARegarderSuivant.add(new Integer(vois));
                                dejaVu[vois] = true;
                                nbDejaVu++;
                                if (nbDejaVu == sizeP - 1) {
                                    pasFini = false;
                                    if (dist > distMax) {
                                        distMax = dist;
                                    }
                                }
                            }
                        }
                    }
                    dist++;
                    voisinARegarderEnCours = new ArrayList();
                    voisinARegarderEnCours = voisinARegarderSuivant;
                    voisinARegarderSuivant = new ArrayList();
                    if (voisinARegarderEnCours.size() == 0) {
                        pasFini = false;
                        if (dist > distMax) {
                            distMax = dist - 1;
                        }
                    }
                }
            }
        }
        // Récupération de la distribution des distances
        int[] distances = new int[distMax + 1];
        Arrays.fill(distances, 0);
        float distanceMoyenne = 0.0f;
        int distanceMax = 0;
        int distanceMin = Integer.MAX_VALUE;
        int nbDistance = 0;
        for (i = 0; i < sizeP - 1; i++) {
            for (j = i + 1; j < sizeP; j++) {
                distances[distanceCouples[i][j]]++;
                distanceMoyenne = distanceMoyenne + (float) distanceCouples[i][j];
                if (distanceMin > distanceCouples[i][j]) {
                    distanceMin = distanceCouples[i][j];
                }
                if (distanceMax < distanceCouples[i][j]) {
                    distanceMax = distanceCouples[i][j];
                }
                nbDistance++;
            }
        }
        distanceMoyenne = distanceMoyenne / (float) nbDistance;
        System.out.println("Distribution des distances deux à deux");
        int p = 0;
        for (i = 0; i < distMax; i++) {
            p = i + 1;
            System.out.print(p + ";");
        }
        System.out.println();
        float[] distancesPartPop = new float[distMax + 1];
        for (i = 0; i < distMax; i++) {
            distancesPartPop[i + 1] = distances[i + 1] / ((float) sizeP * ((float) sizeP - 1f) / 2);
            System.out.print(distancesPartPop[i + 1] + ";");
        }
        System.out.println();
        System.out.print(distanceMoyenne + ";" + distanceMin + ";" + distanceMax + ";");

        // DETERMINATION DE TAILLE DE LA FRONTIERE pour différentes conditions de minorité clustérisée
        float tauxMinCluster[] = {0.1f, 0.2f, 0.3f, 0.4f, 0.5f};
        float partIndivRecense = 0f;
        float partIndivFrontiere = 0f;
        float partIndivFrontierePlusPret, partIndivFrontierePlusLoin = 0f;
        float estimAccroiss = 0.0f;
        //System.out.println("Proportion frontière pour différents taux de minorité négative initiales :") ;
        for (i = 0; i < tauxMinCluster.length; i++) {
            //System.out.print(tauxMinCluster[i]+";") ;   
        }
        //System.out.println("") ;
        for (i = 0; i < tauxMinCluster.length; i++) {
            partIndivRecense = 0f;
            for (j = 0; j < distMax; j++) {
                partIndivRecense = partIndivRecense + distancesPartPop[j + 1];
                if (partIndivRecense > tauxMinCluster[i]) {
                    partIndivRecense = partIndivRecense - distancesPartPop[j + 1];
                    // si i = 0, cas special ou on ne dépasse pas la distance 1
                    if (j == 0) {
                        partIndivFrontiere = tauxMinCluster[i];
                    } else {// si i > 0
                        partIndivFrontierePlusPret = distancesPartPop[j];
                        partIndivFrontierePlusLoin = tauxMinCluster[i] - partIndivRecense;
                        estimAccroiss = distancesPartPop[j + 1] / distancesPartPop[j];
                        partIndivFrontiere = partIndivFrontierePlusLoin + partIndivFrontierePlusPret
                                - (partIndivFrontierePlusLoin / estimAccroiss);
                        System.out.print(partIndivFrontiere + ";");
                    }
                    j = distMax; // on arrête
                }
            }
        }
        //System.out.println() ;
        //*/ //Fin suppression calcul propriété précédente //           int i,j = 0 ;

        // CALCUL DE COEFFICIENT DE CLUSTERING C****************************************
        int nbLiensPossiblesEntreVoisins = 0;
        int nbLiensReelsEntreVoisins = 0;
        int l, k = 0;
        Individu indiv;
        double[] coeffClustLocal = new double[sizeP];
        Arrays.fill(coeffClustLocal, 0.0);
        for (i = 0; i < sizeP; i++) {
            nbLiensPossiblesEntreVoisins = (myPop[i].tailleVoisinage * (myPop[i].tailleVoisinage - 1)) / 2;
            nbLiensReelsEntreVoisins = 0;
            // mesure du nombre de liens réels entre les voisins ;
            for (j = 0; j < myPop[i].tailleVoisinage; j++) {
                indiv = (Individu) (myPop[i].voisins.get(j));
                for (k = j + 1; k < myPop[i].tailleVoisinage; k++) {
                    for (l = 0; l < indiv.tailleVoisinage; l++) {
                        if (indiv.voisins.get(l) == myPop[i].voisins.get(k)) {
                            nbLiensReelsEntreVoisins++;
                            if (nbLiensReelsEntreVoisins > nbLiensPossiblesEntreVoisins) {
                                System.err.println(" CRASSHHH ");
                            }
                        }
                    }
                }
            }
            if (nbLiensReelsEntreVoisins > 0.0) {
                coeffClustLocal[i] = (double) nbLiensReelsEntreVoisins / (double) nbLiensPossiblesEntreVoisins;
            } else {
                coeffClustLocal[i] = 0;
            }
            // Débuggage
            if (coeffClustLocal[i] > 1) {
                System.err.println("	cc local de i = " + i + " : " + coeffClustLocal[i]);
                System.err.println("	Liens " + nbLiensReelsEntreVoisins + " " + nbLiensPossiblesEntreVoisins);
                System.err.println("	taille voisinage " + myPop[i].tailleVoisinage);
                // Edition des voisins de i et des voisins des voisins de i
                for (j = 0; j < myPop[i].tailleVoisinage; j++) {
                    indiv = (Individu) (myPop[i].voisins.get(j));
                    System.err.println("voisin de i : " + indiv.getId());
                    for (l = 0; l < indiv.tailleVoisinage; l++) {
                        System.err.println("voisin de voisin de i : " + ((Individu) indiv.voisins.get(l)).getId());
                    }
                }
            }
        }
        // Calcul du coefficient de clustering moyen
        double sommeCoeffClust = 0.0;
        double minCoeffClust = Double.MAX_VALUE;
        double maxCoeffClust = Double.MIN_VALUE;
        for (i = 0; i < sizeP; i++) {
            sommeCoeffClust = sommeCoeffClust + coeffClustLocal[i];
            if (coeffClustLocal[i] < minCoeffClust) {
                minCoeffClust = coeffClustLocal[i];
            } // Récup Min
            if (coeffClustLocal[i] > maxCoeffClust) {
                maxCoeffClust = coeffClustLocal[i];
            } // Récup Max
        }
        double globalCoef = sommeCoeffClust / (double) sizeP;
        //System.out.println("TypeRes"+";"+typeRes+";"+" m "+";"+resM+";"+" beta "+";"+bruitage+";"+"cc global : "+";"+globalCoef+";"+" min "+";"+minCoeffClust+";"+" max "+";"+maxCoeffClust) ;
        //System.out.println("cc global : "+";"+globalCoef+";"+" min "+";"+minCoeffClust+";"+" max "+";"+maxCoeffClust) ;
        System.out.print(globalCoef + ";" + minCoeffClust + ";" + maxCoeffClust + ";");
    }

    /**
     * Computation of the distribution of popularity
     */
    public float[] getDistribKnownBy() {
        //Method to compute the histogram of percentage of people that 
        //are in a range of percentage of popularity (known by): if you are known by anyone (N-1), you're
        //popularity is 100%
        int[] data = new int[sizeP];
        float[] popularityPerc = new float[sizeP];
        float width = widthClasses;
        int nbClasses = (int) (1 / width);
        float[] result = new float[nbClasses];
        //float a = 0.0f;
        int pos = 0;
        Arrays.fill(data, 0);
        Arrays.fill(result, 0);
        Arrays.fill(popularityPerc, 0);
        /*
         * for (int i = 0; i < sizeP; i++) { for (int j = 0; j <
         * myPop[i].acquaintances.size(); j++) {
         * peopleAreKnownBy[myPop[i].acquaintances.get(j)]++; } }
         */
        for (int i = 0; i < sizeP; i++) {
            //data2[fromWhomIsKnown(i).size()]++;
            data[myPop[i].nbKnownBy]++;
        }
        for (int i = 0; i < sizeP; i++) {
            popularityPerc[i] = (float) ((float) (i) / (float) ((sizeP - 1)));
        }
        /*
         * for (int i = 0; i < sizeP; i++) { System.err.println("Indiv " + i + "
         * knows " + myPop[i].acquaintances); } for (int i = 0; i < sizeP; i++)
         * { System.err.println("Indiv " + i + " is known by " +
         * fromWhomIsKnown(i)); } for (int i = 0; i < sizeP; i++) {
         * System.err.println("Indiv " + i + " is known by #" +
         * fromWhomIsKnown(i).size()); System.err.println("Indiv " + i + " is
         * known by # " + myPop[i].nbKnownBy + " according to nb");
         * //System.err.println("known vector "+myPop[i].knownBy.size()); } for
         * (int i = 0; i < data.length; i++) { System.err.println("Popularity "
         * + i + " people " + data[i]); //System.err.println("Popularity " + i +
         * " people " + data2[i] + " according new method"); }
         */
        for (int i = 0; i < sizeP; i++) {
            /*
             * a = (float) (((float) i) / ((float) sizeP)); if (a < width) {
             * result[pos] = result[pos] + data[i]; //sum = sum + result[pos]; }
             * else { pos++; result[pos] = result[pos] + data[i]; //sum = sum +
             * result[pos]; width = width + 0.05f; }
             */
            while (popularityPerc[i] > width) {
                width = width + widthClasses;
                pos++;
            }
            result[pos] = result[pos] + data[i];
        }
        /*
         * for (int i = 0; i < result.length; i++) { sum = sum + result[i]; }
         */
        /*
         * for (int i = 0; i < nbClasses; i++) { for (int j = 1; j < sizeP + 1;
         * j++) { if ((j > width * i) && (j <= width * (i + 1))) { result[i] =
         * result[i] + data[j]; } } sum = sum + result[i]; }
         */
        for (int i = 0; i < nbClasses; i++) {
            result[i] = (result[i] / (float) sizeP) * 100;
        }
        /*
         * for (int i = 0; i < nbClasses; i++) { System.err.println("%
         * popularity " + i + " % people " + result[i]); }
         */
        return result;
    }

    /**
     * Calcul d'un tableau 2D des densités sur la base d'un pas discrétisation
     * de pasDis
     */
    /*
     * public void calculTabDensite(int replique, int simu) { simu = simu -1 ;
     * float pasDis = 0.04f ; int x = 0 ; int y = 0 ; int size =
     * (int)(2.0f/pasDis) ; float [][] tabDens = new float[size][size] ; int i =
     * 0 ; for (i = 0 ; i < size ; i++) { java.util.Arrays.fill(tabDens[i],
     * 0.0f) ; } // self-esteem only for (i = 0 ; i < sizeP ; i++) { x =
     * Math.round((myPop[i].sBavg[0][0]+1)/pasDis)-1 ; // 1 est le décalage en
     * négatif pour passer de 0 à 2 plutot que de -1 à 1 y =
     * Math.round((myPop[i].sBavg[0][1]+1)/pasDis)-1 ; if (x == -1) { x = 0 ; }
     * if (y == -1) { y = 0 ; } tabDens[x][y]=tabDens[x][y]+1.0f ;
     * //System.err.println("x "+x+" y "+y+" tab "+tabDens[x][y]) ; } // every
     * opinion for (i = 0 ; i < sizeP ; i++) { x =
     * Math.round((myPop[i].sBavg[0][0]+1)/pasDis)-1 ; // 1 est le décalage en
     * négatif pour passer de 0 à 2 plutot que de -1 à 1 y =
     * Math.round((myPop[i].sBavg[0][1]+1)/pasDis)-1 ; if (x == -1) { x = 0 ; }
     * if (y == -1) { y = 0 ; } tabDens[x][y]=tabDens[x][y]+1.0f ;
     * //System.err.println("x "+x+" y "+y+" tab "+tabDens[x][y]) ; }
     * System.out.println("Tableau densite "+";"+simu+";"+replique);
     * //System.out.println() ; for (i = 0 ; i < size ; i++) { for (int j = 0 ;
     * j < size ; j++) { if (tabDens[i][j] > 0.0f) { tabDens[i][j] =
     * tabDens[i][j]/(float)sizeP ; } System.out.print(new String(new
     * Float(tabDens[i][j]).toString()).replace(".",",")+";") ; }
     * System.out.println(); } }
     *
     */
    public String patternDiagnosis() {
        // we have 1 = crisis ; 2 = dominance ; 3 = hierarchy ; 4 = smallworld ; 5 = elite ; 0 is not identified pattern
        String pat = new String("nothing");
        findMaxReputation();
        findMinReputation();
        OpOnHighestAndLowest();
        if (maxReputation < 0) {
            if (maxOpOnMaxReput > 0.5f) {
                if (maxOpOnMinReput > 0.0f) {
                    nbSmallWorld++;
                    return new String("egality");
                }
                nbElite++;
                return new String("elite");
            }
            nbCrisis++;
            return new String("crisis");
        } else {
            if (minOpOnMaxReput < -0.5f) {
                nbElite++;
                return new String("elite");
            }
            if (getNbReputSupHighestAThreshold((maxReputation - 0.5f)) < 3) {
                nbDominance++;
                return new String("dominance");
            }
            nbHierarchy++;
            return new String("hierarchy");
        }
    }

    public void OpOnHighestAndLowest() {
        maxOpOnMaxReput = -1.0f;
        minOpOnMaxReput = 1.0f;
        maxOpOnMinReput = -1.0f;
        for (int i = 0; i < sizeP; i++) {
            if (minOpOnMaxReput > myPop[i].opinion[indivMaxReput]) {
                minOpOnMaxReput = myPop[i].opinion[indivMaxReput];
            }
            if (maxOpOnMaxReput < myPop[i].opinion[indivMaxReput]) {
                maxOpOnMaxReput = myPop[i].opinion[indivMaxReput];
            }
            if (maxOpOnMinReput < myPop[i].opinion[indivMinReput]) {
                maxOpOnMinReput = myPop[i].opinion[indivMinReput];
            }
        }
    }

    public int getNbReputSupHighestAThreshold(float threshold) {
        int nb = 0;
        for (int i = 0; i < sizeP; i++) {
            if (getReputation(i) > threshold) {
                nb++;
            }
        }
        return nb;
    }

    /**
     * Permet de générer un réseau particulier (réseau d'amour)
     */
    public void computePosOpinionNetwork() {
        for (int i = 0; i < myPop.length; i++) {
            myPop[i].voisinsPos = new ArrayList();
        }
        for (int i = 0; i < myPop.length; i++) {
            for (int j = 0; j < myPop.length; j++) {
                if (myPop[i].opinion[j] > 0.0f) { // conditions is a positive opinion of i for j
                    if (i != j) {
                        myPop[i].voisinsPos.add(myPop[j]);
                    }
                }
            }
            myPop[i].tailleVoisinagePos = myPop[i].voisinsPos.size();
        }
    }

    /**
     * Permet de générer un réseau particulier (réseau d'estime)
     */
    public void computeEsteemOpinionNetwork() {
        for (int i = 0; i < myPop.length; i++) {
            myPop[i].voisinsPos = new ArrayList();
        }
        for (int i = 0; i < myPop.length; i++) {
            for (int j = 0; j < myPop.length; j++) {
                if (i != j) {
                    if (myPop[i].opinion[j] > myPop[i].opinion[i]) { // conditions is an esteem
                        myPop[i].voisinsPos.add(myPop[j]);
                    }
                }
            }
            myPop[i].tailleVoisinagePos = myPop[i].voisinsPos.size();
        }
    }

    /**
     * Permet de générer un réseau particulier (réseau "Je pense qu'il
     * m'estime")
     *
     */
    public void computeIsEsteemedByOpinionNetwork() {
        for (int i = 0; i < myPop.length; i++) {
            myPop[i].voisinsPos = new ArrayList();
        }
        for (int i = 0; i < myPop.length; i++) {
            for (int j = 0; j < myPop.length; j++) {
                if (i != j) {
                    if (myPop[i].opinion[i] > myPop[i].opinion[j]) { // I think j esteems me
                        myPop[i].voisinsPos.add(myPop[j]);
                    }
                }
            }
            myPop[i].tailleVoisinagePos = myPop[i].voisinsPos.size();
        }
    }

    /**
     * Permet de générer un réseau particulier (réseau "Je pense qu'ils valent
     * comme moi aii + ou - delta")
     *
     */
    public void computeAffinityOpinionNetwork() {
        for (int i = 0; i < myPop.length; i++) {
            myPop[i].voisinsPos = new ArrayList();
        }
        for (int i = 0; i < myPop.length; i++) {
            for (int j = 0; j < myPop.length; j++) {
                if (Math.abs(myPop[i].opinion[j] - myPop[i].opinion[i]) <= parameters.getDelt()) { // I think j esteems me
                    if (i != j) {
                        myPop[i].voisinsPos.add(myPop[j]);
                    }
                }
            }
            myPop[i].tailleVoisinagePos = myPop[i].voisinsPos.size();
        }
    }

    /**
     * Permet de générer un réseau particulier réseau Affinity réciproque)
     *
     */
    public void computeRecAffinityOpinionNetwork() {
        for (int i = 0; i < myPop.length; i++) {
            myPop[i].voisinsPos = new ArrayList();
        }
        for (int i = 0; i < myPop.length; i++) {
            for (int j = i + 1; j < myPop.length; j++) {
                if (Math.abs(myPop[i].opinion[j] - myPop[i].opinion[i]) <= parameters.getDelt()
                        && Math.abs(myPop[j].opinion[i] - myPop[j].opinion[j]) <= parameters.getDelt()) {
                    if (i != j) {
                        myPop[i].voisinsPos.add(myPop[j]);
                        myPop[j].voisinsPos.add(myPop[i]);
                    }
                }
            }
            myPop[i].tailleVoisinagePos = myPop[i].voisinsPos.size();
        }
    }

    /**
     * Mesure si les opinions sur un individus sont consensuelles et peuvent
     * être prise comme des réputations
     */
    public float consensuelOpinion() {
        float distMinMax = 0.0f;
        for (int i = 0; i < sizeP; i++) {
            float minOpOnI = 1.0f;
            float maxOpOnI = -1.0f;
            for (int j = 0; j < sizeP; j++) {
                if (i != j) {
                    if (myPop[j].opinion[i] < minOpOnI) {
                        minOpOnI = myPop[j].opinion[i];
                    }
                    if (myPop[j].opinion[i] > maxOpOnI) {
                        maxOpOnI = myPop[j].opinion[i];
                    }
                }
            }
            distMinMax = distMinMax + (maxOpOnI - minOpOnI);
            //System.err.println("i "+i+" min  "+minOpOnI+" max "+maxOpOnI+" distMinMax "+distMinMax) ;
        }
        distMinMax = distMinMax / (float) sizeP;
        return distMinMax;
    }

    /**
     * Donne la distribution des réputations
     */
    public int[] reputationDistrib() {
        int taille = 8;
        float bound = 2.0f / (float) taille;
        int[] histo = new int[taille]; // une intervalle de taille 0.5
        java.util.Arrays.fill(histo, 0);
        float j = 0.0f;
        float debut = -1;
        float fin = 1;
        float borne = 0.0f;
        float reput = 0.0f;
        int i = 0;
        int k = 0;
        for (i = 0; i < sizeP; i++) {
            reput = getReputation(i);
            k = 0;
            for (j = debut; j < fin; j = j + bound) {
                borne = j + bound;
                if (j == debut && reput == debut) {
                    histo[k]++;
                } else {
                    if (reput <= borne) {
                        if (reput > j) {
                            histo[k]++;
                        }
                    }
                }
                k++;
            }
        }
        float part = 0.0f;
        for (i = 0; i < taille; i++) {
            part = (float) histo[i] / (float) sizeP;
            //System.out.print(((new Float(part)).toString()).replace(".", ",") + ";");
        }
        return histo;
    }

    public float[] slopeReputDistrib() {
        float slope = 0.0f;
        float slopeMin = 100000000000.0f;
        float slopeMax = 0.0f;
        int[] hist = reputationDistrib();
        float s = 0.0f;
        for (int i = 1; i < hist.length; i++) {
            s = Math.abs(hist[i] - hist[i - 1]);
            slope = slope + s;
            if (s > slopeMax) {
                slopeMax = s;
            }
            if (s < slopeMin) {
                slopeMin = s;
            }
        }
        float[] res = new float[3];
        res[0] = slope;
        res[1] = slopeMin;
        res[2] = slopeMax;
        return res;
        // slope = 0 means the distribution is uniform (should be the case for hierarchy)
    }

    public void minmaxOpDeSoi() {
        minOpSoi = 2.0f;
        maxOpSoi = -2.0f;
        for (int i = 0; i < sizeP; i++) {
            if (myPop[i].opinion[i] > maxOpSoi) {
                maxOpSoi = myPop[i].opinion[i];
            }
            if (myPop[i].opinion[i] < minOpSoi) {
                minOpSoi = myPop[i].opinion[i];
            }
        }
    }

    /**
     * Give nb having positive reputation of the individual
     */
    int nbPosReputation;
    float minReputPos;
    float maxReputNeg;
    float moyDifOp;
    float moyDifOpOfLeader;
    int nbPosOp;

    public void getIndicateurs() {
        nbPosReputation = 0;
        float maxReputPos = -1.0f;
        int leader = -1;
        minReputPos = 1.0f;
        maxReputNeg = -1.0f;
        float reput = 0.0f;
        for (int i = 0; i < myPop.length; i++) {
            reput = getReputation(i);
            if (reput > 0.0f) {
                nbPosReputation++;
                if (reput < minReputPos) {
                    minReputPos = reput;
                }
                if (reput > maxReputPos) {
                    leader = i;
                }
            } else {
                if (reput > maxReputNeg) {
                    maxReputNeg = reput;
                }
            }
        }
        moyDifOp = difMoyenneSurOpinions(leader);
        nbPosOp = nbPositiveOpinion();
    }

    public float difMoyenneSurOpinions(int l) {
        float moyDifOpinAll = 0.0f;
        moyDifOpOfLeader = 0.0f;
        for (int i = 0; i < sizeP; i++) {
            moyDifOpinAll = moyDifOpinAll + difOpD1Indiv(i);
        }
        moyDifOpinAll = moyDifOpinAll / (float) sizeP;
        if (l > 0) {
            moyDifOpOfLeader = difOpD1Indiv(l);
        }
        return moyDifOpinAll;
    }

    public float difOpD1Indiv(int i) {
        int nbDist = 0;
        float moyDifOpin = 0.0f;
        for (int k = 0; k < sizeP; k++) {
            for (int j = k + 1; j < sizeP; j++) {
                moyDifOpin = moyDifOpin + Math.abs(myPop[i].opinion[k] - myPop[i].opinion[j]);
                nbDist++;
            }
        }
        moyDifOpin = moyDifOpin / (float) nbDist;
        return moyDifOpin;
    }

    public int nbPositiveOpinion() {
        int nbPositOp = 0;
        int nbDist = 0;
        float moyDifOpin = 0.0f;
        for (int k = 0; k < sizeP; k++) {
            for (int j = 0; j < sizeP; j++) {
                if (myPop[k].opinion[j] > 0.0f) {
                    nbPositOp++;
                }
            }
        }
        moyDifOpin = moyDifOpin / (float) nbDist;
        return nbPositOp;
    }

    public void characteriseReputation(String expeLot, float expe, float replique, long time) {
        computeStatistics();
        float isReput = consensuelOpinion();
        float[] slopeDist = slopeReputDistrib();
        mod.ps9.print(expeLot + ";");
        printParametres(mod.ps9);
        mod.ps9.print(time + ";");
        mod.ps9.print(new Float(isReput).toString().replace(".", ",") + ";");
        mod.ps9.print(new Float(slopeDist[0]).toString().replace(".", ",") + ";");
        mod.ps9.print(new Float(slopeDist[1]).toString().replace(".", ",") + ";");
        mod.ps9.print(new Float(slopeDist[2]).toString().replace(".", ",") + ";");
        getIndicateurs();
        mod.ps9.print(nbPosReputation + ";");
        mod.ps9.print(nbSelfLovers + ";");
        mod.ps9.print(nbNegOpinion + ";");
        //minmaxOpinion();
        minmaxOpDeSoi();
        mod.ps9.print(new Float(minOpSoi).toString().replace(".", ",") + ";");
        mod.ps9.print(new Float(maxOpSoi).toString().replace(".", ",") + ";");
        //mod.ps9.print(new Float(minOp).toString().replace(".", ",") + ";");
        //mod.ps9.print(new Float(maxOp).toString().replace(".", ",") + ";");
        //mod.ps9.print(new Float(minReputPos).toString().replace(".", ",") + ";");
        //mod.ps9.print(new Float(maxReputNeg).toString().replace(".", ",") + ";");
        //mod.ps9.print(new Float(moyDifOp).toString().replace(".", ",") + ";");
        //mod.ps9.print(new Float(moyDifOpOfLeader).toString().replace(".", ",") + ";");
        mod.ps9.print(nbPosOp + ";");
        computeSelfBiais();
        //mod.ps9.print(new Float(forceDefenseSoi).toString().replace(".", ",") + ";");
        //mod.ps9.print(new Float(forceInfluence).toString().replace(".", ",") + ";");
        /*
         * for (int i = 0; i < myPop.length; i++) { mod.ps9.print(new
         * Float(getReputation(i)).toString().replace(".", ",") + ";"); } for
         * (int i = 0; i < myPop.length; i++) { mod.ps9.print(new
         * Float(myPop[i].opinion[i]).toString().replace(".", ",") + ";"); } for
         * (int i = 0; i < myPop.length; i++) { // rang que i s'attribue int
         * audessus = 0 ; for (int j = 0 ; j < myPop.length ; j++) { if
         * (myPop[i].opinion[j] > myPop[i].opinion[i]) { audessus++ ; } }
         * mod.ps9.print(new Float(audessus+1).toString().replace(".", ",") +
         * ";"); }
         *
         */
        /*
         * int[] hist = reputationDistrib(); System.err.println(" distribution
         * réputation"); for (int i = 0; i < hist.length; i++) {
         * System.err.print(hist[i] + ";"); } System.err.println();
         *
         */
    }

    // COMPARER POSITION DONNEE PAR LES AUTRES A POSITION QU'IL SE DONNE
    // nb indiv que j'estime (meilleur que moi)
    // nb fois où 0 individus estimés
    public void computeSelfBiais() {
        int nbEstimes = 0;
        int nbMoiPlusEstime = 0;
        int cumulNbEstimes = 0;
        float deltaPerceptPos = 0.0f;
        int nbSurEvalue = 0;
        int nbSousEvalue = 0;
        int wellEvalue = 0;
        for (int indiv = 0; indiv < sizeP; indiv++) {
            nbEstimes = 0;
            deltaPerceptPos = 0.0f;
            for (int i = 0; i < sizeP; i++) {
                if (myPop[indiv].opinion[i] > myPop[indiv].opinion[indiv]) {
                    nbEstimes++;
                }
            }
            if (nbEstimes == 0) {
                nbMoiPlusEstime++;
            }
            cumulNbEstimes = cumulNbEstimes + nbEstimes;
            /*
             * deltaPerceptPos = nbEstimes - posIndiv(indiv) ; if
             * (deltaPerceptPos > 0.0f) { nbSousEvalue++ ; } else { if
             * (deltaPerceptPos < 0.0f) { nbSurEvalue++ ; } else { wellEvalue++
             * ; } }
             */
            //System.err.println(deltaPerceptPos) ;

        }
        //System.err.println("surEvalue "+nbSurEvalue+" sousEvalue "+nbSousEvalue+" well evalue "+wellEvalue) ;
        //float parSurEvalue = (float)nbSurEvalue/(float)sizeP ;
        //System.err.println("part se surévaluant "+"\t"+parSurEvalue);
        float nbEstimesMoyen = (float) cumulNbEstimes / (float) sizeP;
        mod.ps9.print(nbMoiPlusEstime + ";");
        mod.ps9.print(nbEstimesMoyen + ";");
    }

    public float posIndiv(int indiv) {
        int nbEst = 0;
        float nbEstAv = 0.0f;
        for (int i = 0; i < sizeP; i++) {
            for (int j = 0; j < sizeP; j++) {
                if (myPop[i].opinion[j] > myPop[i].opinion[indiv]) {
                    nbEst++;
                }
            }
        }
        nbEstAv = (float) nbEst / (float) sizeP;
        return nbEstAv;
    }

} // Fin de la classe

