package fr.irstea.lisc.leviathan;


import java.util.ArrayList;
import java.util.Arrays;

/**
 * Classe decrivant un individu de la population et calculant son comportement
 */
public class Individu {

    public boolean rencontrable ;
    /**
     * identifiant de l'individu
     */
    public int id;

    public int getId() {
        return this.id;
    }

    public void setId(int value) {
        this.id = value;
    }
    /**
     * Attributes for the leviathan model
     */
    float[] opinion;
    float ro; // propension d'un individu à écouter autrui
    float w;
    float boundForImpression; // maximum value (negative or positive) the individual can give as a impression (management of the context effect)
    int k;
    ArrayList<Integer> acquaintances;
    int[] distanceFromOthers; // vector which has at position j the distance from i to j
    //ArrayList<Integer> isKnownBy ;
    int nbKnownBy = 0; // from how many people a single individual is known
    public boolean leader;
    float loveForMe; // averageReputation love of the others for the individual
    int timeLeadership;
    /**
     * voisinage de l'individu
     */
    public ArrayList voisins = new ArrayList();
    public int tailleVoisinage;

    public ArrayList getVoisins() {
        return this.voisins;
    }
    public void setVoisins(ArrayList value) {
        this.voisins = value;
        tailleVoisinage = voisins.size();
    }
    public int group ; // identifiant du groupe
    
   /**
     * voisinage de l'individu constitué par les liens d'opinions réciproques (d'estime ou d'amour)
     */
    public ArrayList voisinsPos = new ArrayList();
    public int tailleVoisinagePos;

    public ArrayList getVoisinsPos() {
        return this.voisinsPos;
    }
    public void setVoisinsPos(ArrayList value) {
        this.voisinsPos = value;
        tailleVoisinagePos = voisinsPos.size();
    }
    
    
    
    /**
     * Getter for delta (= boundForImpression)
     */
    public float getBoundForImpression() {
        return boundForImpression;
    }

    /**
     * Constructor for the leviathan model
     */
    public Individu(Population pop, float omega, int ka, float rho, float delta, int ident) {
        // initialisation of the opinions
        this.boundForImpression = delta;
        this.k = ka;
        opinion = new float[pop.sizeP];
        this.w = omega;
        this.ro = rho;
        setId(ident);
        acquaintances = new ArrayList<Integer>();
        distanceFromOthers = new int [pop.sizeP];
        //isKnownBy = new ArrayList<Integer>();
        leader = false;
        timeLeadership = 0;
        loveForMe = 0;
        Arrays.fill(opinion, -5.0f);
        Arrays.fill(distanceFromOthers, 0);
    }

    /**
     * Add acquaintance memory
     */
    public void addAcquaintance(Integer i) {
        if (i.intValue() != getId()) {
            acquaintances.add(i);
            opinion[i.intValue()] = 0.0f;
        }
    }

    /**
     * Remove acquaintance from memory: i is the identifier of the individual to remove, j is its position in the acquaintance vector
     */
    public void removeAcquaintance(int i, int j) {
        opinion[i] = -5.0f;
        acquaintances.remove(j);
    }

    ///////////////////////////// PARTIE DE CODAGE DE RENVOI DES VALEURS ///////////////////////
    /**
     * Renvoie l'état du voisinage de l'individu
     */
    void monVoisinage() {
        System.out.print(id + "\t");
        for (int i = 0; i < tailleVoisinage; i++) {
            for (int j = i + 1; j < tailleVoisinage; j++) {
                if (((Individu) (getVoisins().get(i))).getId() == ((Individu) (getVoisins().get(j))).getId()) {
                    System.err.println("Indiv " + getId() + " a deux fois " + ((Individu) (getVoisins().get(i))).getId() + " ");
                }
            }
        }
    }

    /**
     * Tell if the individual identified by indiv is memorized (i.e. known)
     */
    public boolean knows(int indiv) {
        boolean know = false;
        if (indiv == getId()) {
            know = true;
        } else {
            for (int r = 0; r < acquaintances.size(); r++) {
                if (acquaintances.get(r).intValue() == indiv) {
                    know = true;
                    r = acquaintances.size();
                }
            }
        }
        return know;
    }
} // Fin de la classe
