package fr.irstea.lisc.leviathan;


/**
 * classe permettant de lancer des simulations pour plusieurs jeux de param�tres
 */
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.util.*;
import java.io.*;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

class MultiSimParam {

    Vector jeuxParam = new Vector();
    Vector dossierResult = new Vector();
    PrintStream console = System.out;
    PrintStream ps1;
    FileOutputStream fic1;
    PrintStream ps2;
    FileOutputStream fic2;
    PrintStream ps9;
    FileOutputStream fic9;

    MultiSimParam(String fileName, String nomFichierSortie, String toto, String leader, String histogram, String avalanche, int nLDebut, int nLFin) {
        boolean entete = true;
        for (int i = nLDebut; i <= nLFin; i++) {
            new MonModele(fileName, nomFichierSortie, toto, leader, histogram, avalanche, i, entete);
            entete = false;
            System.err.println("jeux de parametres num " + (i - nLDebut + 1) + " fini");
        }
    }

    static int nombreLignes(String fileName) {
        int nLignes = 0;
        int i = 0;
        try {
            FileReader file = new FileReader(fileName);
            StreamTokenizer st = new StreamTokenizer(file);
            while (st.nextToken() != st.TT_EOF)
                ;
            nLignes = st.lineno();
            file.close();
        } catch (Exception e) {
            System.out.println("erreur lecture : " + e.toString());
        }

        return nLignes - 1;
    }

    MultiSimParam(String fileName, String dataForDataBase, String parameter, String net, int nLDebut, int nLFin) {
        boolean entete = true;
        console.println("lancons la simulation avec le fichier " + fileName + " le fichier resultat est " + dataForDataBase + " la liste des expes est dans " + parameter + " fichier network properties " + net);
        createOutputFile(dataForDataBase, parameter, net);
        for (int i = nLDebut; i <= nLFin; i++) {
            new MonModele(fileName, i, entete, ps1, ps2, ps9);
            entete = false;
            System.err.println("jeux de parametres numero " + (i - nLDebut + 1) + " fini");
        }
        try {
            fic1.close();
            ps1.close();
            fic2.close();
            ps2.close();
            fic9.close();
            ps9.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("probleme de fermeture du fichier");
        }

    }

    /*
     * nouvelle version : -i : nom du fichier de param
     *
     */
    static boolean marqueur(String s) {
        return ((s.substring(0, 1)).equals("-"));
    }

    static void messageParam() {
        System.out.println("#### Rappel : les parametres sont :");
        System.out.println("\t nom du fichier de parametres (+ 1ere (+ derniere lignes))");
        System.out.println("\t -o : nom du fichier r�sultats (optionnel), require three names ");
        //System.out.println("\t -f : fr�quence de sauvegarde (optionnel)");
    }

    public static void main(String[] args) {
        // lecture des param�tres
        int icour = 0;
        String fichierParam = new String("");
        int nLDeb = 2; // lignes de d�but et de fin
        int nLFin = 2;
        int freqSvg = 1; // fr�quence des sauvegardes
        String fichierResult1 = new String("");
        String fichierResult2 = new String("");
        String fichierResult3 = new String("");
        //String fichierResult4 = new String("");
        //String fichierResult5 = new String("");
        int nargs = args.length;
        boolean problem = false;
        // on lit le nom du fichier de param et �ventuellement les premi�re et
        // derni�re lignes
        if (icour < nargs && !marqueur(args[icour])) {
            fichierParam = args[icour];
            if (++icour < nargs && !marqueur(args[icour])) {
                nLDeb = (Integer.valueOf(args[icour])).intValue();
                if (++icour < nargs && !marqueur(args[icour])) {
                    nLFin = (Integer.valueOf(args[icour++])).intValue();
                } else {
                    nLFin = nombreLignes(fichierParam);
                }
            } else {
                nLFin = nombreLignes(fichierParam);
            }
            while (icour < nargs) {
                if (args[icour].equals("-o")) {
                    if (++icour < nargs && !marqueur(args[icour])) {
                        fichierResult1 = args[icour++];
                        fichierResult2 = args[icour++];
                        fichierResult3 = args[icour++];
                        //fichierResult4 = args[icour++];
                        //fichierResult5 = args[icour++];
                    } else {
                        problem = true;
                    }
                } else {
                    if (args[icour].equals("-f")) {
                        if (++icour < nargs && !marqueur(args[icour])) {
                            freqSvg = (Integer.valueOf(args[icour++])).intValue();
                        } else {
                            problem = true;
                        }
                    }
                }
            }
        } else {
            problem = true;
        }
        if (fichierParam.length() == 0 || problem) {
            messageParam();
        } else {
            //new MultiSimParam(fichierParam, fichierResult1, fichierResult2, fichierResult3, fichierResult4, fichierResult5, nLDeb, nLFin);
            new MultiSimParam(fichierParam, fichierResult1, fichierResult2, fichierResult3, nLDeb, nLFin);
        }
        //*/
    }

    /**
     * méthode d'écriture des résultats de la simulation, on écrit la valeur des
     * objets du modele
     */
    public void createOutputFile(String nameResult, String toto, String net) {
        try {
            File file1 = new File(nameResult);
            File file2 = new File(toto);
            File file9 = new File(net);
            if (file1.exists()) {
                String msg = "<html><body>The output directory <b>" + file1.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            if (file2.exists()) {
                String msg = "<html><body>The output directory <b>" + file2.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            if (file9.exists()) {
                String msg = "<html><body>The output directory <b>" + file9.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            file1.getParentFile().mkdirs();
            file2.getParentFile().mkdirs();
            file9.getParentFile().mkdirs();
            fic1 = new FileOutputStream(file1);
            ps1 = new PrintStream(fic1);
            fic2 = new FileOutputStream(file2);
            ps2 = new PrintStream(fic2);
            fic9 = new FileOutputStream(file9);
            ps9 = new PrintStream(fic9);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println();
            System.out.println("j'ai un problème d'écriture des résultats");
        }

    }
} // Fin de la classe
