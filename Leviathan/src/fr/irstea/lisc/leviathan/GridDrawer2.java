package fr.irstea.lisc.leviathan;

//package fr.cemagref.commons.draw2d;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GridDrawer2 implements MouseMotionListener {

    private int width, height;
    private transient JComponent display;
    private transient JLabel label;
    private transient Color[][] colorsGrid;
    private transient float[][] poids;
    private transient String[][] descriptionsGrid;
    private Color emptyColor;
    public float points[][];
    private transient int[] aPeindreEnNoir;
    private transient int[] aEffacer;
    private transient int displayWidthBak, displayHeightBak;
    private transient double cellSizeX, cellSizeY;

    public GridDrawer2(int width, int height, Population myPop) {
        this(width, height, Color.WHITE);
        aPeindreEnNoir = new int[myPop.sizeP * 2];
        aEffacer = new int[myPop.sizeP * 2];
    }

    public GridDrawer2(int width, int height, Color emptyColor) {
        super();
        this.width = width / 2;
        this.height = height / 2;
        this.emptyColor = emptyColor;
        this.init();
    }

    public void init() {
        // init display component
        display = new JPanel(new BorderLayout());
        DisplayComponent displayComponent = new DisplayComponent();
        displayComponent.addMouseMotionListener(this);
        displayComponent.setVisible(true);
        displayComponent.setDoubleBuffered(true);
        display.setBackground(Color.WHITE);
        label = new JLabel("");
        display.add(displayComponent, BorderLayout.CENTER);
        display.add(label, BorderLayout.PAGE_START);
        // init display variables
        ///*
        colorsGrid = new Color[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                colorsGrid[i][j] = emptyColor;
            }
        }
        //*/
        float[][] poids = new float[width][height];
        for (int i = 0; i < width; i++) {
            Arrays.fill(poids[i], 0.0f);
        }
        descriptionsGrid = new String[width][height];
        displayWidthBak = 0;
        displayHeightBak = 0;
        points = new float[width][height];
        for (int i = 0; i < width; i++) {
            Arrays.fill(points[i], -1000);
        }
        // show the result
        display.repaint();
    }

    /**
     * Set the color for the cell at (x,y).
     *
     * @param x
     * @param y
     * @param color
     */
    public void drawColorAt(int x, int y, Color color) {
        try {
            if (x == width) {
                x = width - 1;
            }
            if (y == height) {
                y = height - 1;
            }
            colorsGrid[x][y] = color;
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println("y'a un probleme " + e + " x " + x + " y " + y + " " + color);
        }
    }

    /**
     * Set the color for the cell at (x,y).
     *
     * @param x
     * @param y
     * @param color
     */
    public void drawColorPoidsAt(int x, int y, Color color, float poids) {
        try {
            if (x == width) {
                x = width - 1;
            }
            if (y == height) {
                y = height - 1;
            }
            colorsGrid[x][y] = color;
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println("y'a un probl�me " + e + " x " + x + " y " + y);
        }
    }

    /**
     * Remove any color setted at (x,y). The default color for empty cells will
     * be used.
     *
     * @param x
     * @param y
     */
    public void eraseColorAt(int x, int y) {
        colorsGrid[x][y] = emptyColor;
    }

    /**
     * Set a description for the cell at (x,y). This description will be
     * displayed above the grid when the mouse will move over this cell.
     *
     * @param x
     * @param y
     * @param description
     */
    public void setDescriptionAt(int x, int y, String description) {
        descriptionsGrid[x][y] = description;
    }

    /**
     * @return The object ready to be displayed into a frame.
     */
    /**
     * @return The object ready to be displayed into a frame. AFFICHAGE INDIVIDU
     * OPTIMISE
     */
    public JComponent exexgetDisplay(Population mymodel, int objet) {
        int x, y = 0;
        int z = 0;
        Arrays.fill(aEffacer, 0);
        for (int i = 0; i < mymodel.sizeP; i++) {
            for (int j = 0; j < mymodel.sizeP; i++) {
                x = Math.round(((float) ((mymodel.myPop[i].opinion[j] + 1) * 0.5 * width))); //100
                y = (-Math.round((float) ((mymodel.myPop[j].opinion[i] + 1) * 0.5 * height)) + height); //100
                if (aPeindreEnNoir[i * 2] != x) {
                    aEffacer[z] = 1;
                    aEffacer[z + 1] = 1;
                    z++;
                } else {
                    if (aPeindreEnNoir[i * 2 + 1] != y) {
                        aEffacer[z] = 1;
                        aEffacer[z + 1] = 1;
                        z++;
                    }
                }
                aPeindreEnNoir[i * 2] = x;
                aPeindreEnNoir[i * 2 + 1] = y;
                // Ou faire un tableau "� effacer"
                // et un tableau "� peindre en noir"
            }
        }
        return display;
    }

    /**
     * @return The object ready to be displayed into a frame. AFFICHAGE DENSITE
     */
    public JComponent ExgetDisplay(Population mymodel, int objet) {
        /*
         * /
         * for(int i=0;i < width ;i++){ for(int j=0;j < height ;j++){
         * eraseColorAt(i, j); } }
         */
        poids = new float[width][height];
        for (int i = 0; i < width; i++) {
            Arrays.fill(poids[i], 0.0f);
        }
        int x, y = 0;
        int z = 0;
        Arrays.fill(aEffacer, 0);
        //affichage en densit�
        boolean[] indivVu = new boolean[mymodel.sizeP];
        Arrays.fill(indivVu, false);
        ArrayList stockXY[] = new ArrayList[mymodel.sizeP];
        for (int i = 0; i < mymodel.sizeP; i++) {
            stockXY[i] = new ArrayList();
        }
        for (int i = 0; i < mymodel.sizeP; i++) {
            for (int j = 0; j < mymodel.sizeP; j++) {
                x = Math.round(((float) ((mymodel.myPop[i].opinion[j] + 1) * 0.5 * width))); //100
                y = (-Math.round((float) ((mymodel.myPop[j].opinion[i] + 1) * 0.5 * height)) + height); //100
                if (x == width) {
                    x = width - 1;
                }
                if (y == height) {
                    y = height - 1;
                }
                stockXY[i].add(new Integer(x)); // affichage en densite
                stockXY[i].add(new Integer(y)); // affichage en densite
                /*
                 * if (aPeindreEnNoir[i * 2] != x) { aEffacer[z] = 1; aEffacer[z
                 * + 1] = 1; z++; } else { if (aPeindreEnNoir[i * 2 + 1] != y) {
                 * aEffacer[z] = 1; aEffacer[z + 1] = 1; z++; } }
                 */
                aPeindreEnNoir[i * 2] = x;
                aPeindreEnNoir[i * 2 + 1] = y;
                // Ou faire un tableau "� effacer"
                // et un tableau "� peindre en noir"
            }
        }
        int compte = 0;
        //float poids = 0.0f ;
        for (int i = 0; i < mymodel.sizeP; i++) {
            if (!indivVu[i]) {
                compte = 1;
                indivVu[i] = true;
                for (int j = i + 1; j < mymodel.sizeP; j++) {
                    if (((Integer) stockXY[i].get(0)).intValue() == ((Integer) stockXY[j].get(0)).intValue()) {
                        if (((Integer) stockXY[i].get(1)).intValue() == ((Integer) stockXY[j].get(1)).intValue()) {
                            poids[((Integer) stockXY[i].get(0)).intValue()][((Integer) stockXY[i].get(1)).intValue()]++;
                            indivVu[j] = true;
                        }
                    }
                }
                poids[((Integer) stockXY[i].get(0)).intValue()][((Integer) stockXY[i].get(1)).intValue()]
                        = (poids[((Integer) stockXY[i].get(0)).intValue()][((Integer) stockXY[i].get(1)).intValue()] / (float) mymodel.sizeP * 100);
            }
        }

        return display;
    }

    /**
     * @return The object ready to be displayed into a frame. AFFICHAGE INDIVIDU
     * (OPTIMISER, TOUT N'EST PAS REBLANCHI AVANT L'AFFICHAGE
     */
    public JComponent getDisplay(Population mymodel) {
        int x, y = 0;
        int i = 0;
        for (i = 0; i < width; i++) {
            Arrays.fill(points[i], -1000);
        }
        for (i = 0; i < mymodel.sizeP; i++) {
            for (int j = 0; j < mymodel.sizeP; j++) {
                x = (int) (i * width / mymodel.sizeP); //100
                y = (int) -(j * height / mymodel.sizeP) + height - 1; //100
                //System.err.println("i "+i+" j "+j+" x "+x+" y "+y+" width "+width+" height "+height) ;
                /*
                 * if (y == height) { y = 0; } if (x == width) { x = width - 1;
                 * } if (y == -1) { y = height - 1; } if (x == -1) { x = 0; }
                 *///
                points[x][y] = mymodel.myPop[i].opinion[j];
            }
        }
        /*
         * for (i = 0; i < width; i++) { for (int j = 0; j < height; j++) {
         * //System.err.println("i " + i + " j " + j + " point " +
         * points[i][j]); drawColorAt(i, j, emptyColor); if (points[i][j] < 0) {
         * drawColorAt(i, j, Color.BLACK); } else { if (points[i][j] > 0) {
         * drawColorAt(i, j, Color.RED); } } } }
         *
         */
        return display;
    }

    private class DisplayComponent extends JComponent {

        protected synchronized void paintComponent(Graphics g) {
            super.paintComponents(g);
            Graphics2D g2d = (Graphics2D) g;
            int x, y = 0;
            // determine if generalPath must be rescaled
            if ((this.getWidth() != displayWidthBak) || (this.getHeight() != displayHeightBak)) {
                // backup for comparaison in the next loop
                displayWidthBak = this.getWidth();
                displayHeightBak = this.getHeight();
                cellSizeX = displayWidthBak / (width);
                cellSizeY = displayHeightBak / (height);
                System.out.println(displayWidthBak + " " + displayHeightBak);
            }
            float emphaseur = 10.0f;
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    y = (i * (int) cellSizeX) + 4;
                    x = (j * (int) cellSizeY) + 4;
                    //System.err.println(points[i][j]) ;
                    if (points[i][j] == 0.0f) {
                        g.setColor(Color.white);
                    } else {
                        if (points[i][j] < 0.0f) { // couleurs bleues pour les négatifs
                            if (points[i][j] < -1.05) {
                                g.setColor(Color.white);
                            } else if (points[i][j] < -0.75) {
                                g.setColor(new Color(51.0f / 255.0f, 51.0f / 255.0f, 255.0f / 255.0f));
                            } else {
                                if (points[i][j] < -0.5) {
                                    g.setColor(new Color(51.0f / 255.0f, 102.0f / 255.0f, 255.0f / 255.0f));
                                } else {
                                    if (points[i][j] < -0.25) {
                                        g.setColor(new Color(51.0f / 255.0f, 204.0f / 255.0f, 255.0f / 255.0f));
                                    } else {
                                        g.setColor(new Color(102.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f));
                                    }
                                }
                            }
                        } //* 
                        //*/
                        else { // couleurs rouges pour les positifs
                            if (points[i][j] > 0.75) {
                                g.setColor(new Color(204.0f / 255.0f, 0.0f, 0.0f));
                            } else {
                                if (points[i][j] > 0.5) {
                                    g.setColor(new Color(255.0f / 255.0f, 0.0f, 0.0f));
                                } else {
                                    if (points[i][j] > 0.25) {
                                        g.setColor(new Color(255.0f / 255.0f, 102.0f / 255.0f, 102.0f / 255.0f));
                                    } else {
                                        g.setColor(new Color(255.0f / 255.0f, 204.0f / 255.0f, 204.0f / 255.0f));
                                    }
                                }
                            }
                        }
                    }

                    g.fillRect(x, y,
                            (int) (cellSizeX
                            * emphaseur), (int) (cellSizeY
                            * emphaseur));
                }
            }
            /*
             */
            //try {
                /*
             * Suivre trois individus particuliers / g.setColor(Color.RED); x =
             * (aPeindreEnNoir[0]*(int)cellSizeX)+4 ; y =
             * (aPeindreEnNoir[1]*(int)cellSizeY)+4 ; g.fillRect(x, y,
             * 3*(int)cellSizeX, 3*(int)cellSizeY); g.setColor(Color.ORANGE); x
             * = (aPeindreEnNoir[2]*(int)cellSizeX)+4 ; y =
             * (aPeindreEnNoir[3]*(int)cellSizeY)+4 ; g.fillRect(x, y,
             * 3*(int)cellSizeX, 3*(int)cellSizeY); g.setColor(Color.BLUE); x =
             * (aPeindreEnNoir[4]*(int)cellSizeX)+4 ; y =
             * (aPeindreEnNoir[5]*(int)cellSizeY)+4 ; g.fillRect(x, y,
             * 3*(int)cellSizeX, 3*(int)cellSizeY);
             *
             * } catch (Exception e) { // TODO: handle exception }
             */
        }
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
        int y = (int) (e.getY() / cellSizeY);
        int x = (int) (e.getX() / cellSizeX);
        float t = ((float) x / 100 * 2) - 1;
        float z = -((float) y / 100 * 2) + 1;
        label.setText(t + " ; " + z);
        /*
         * if ((x>=0)&&(x<width)&&(y>=0)&&(y<height)) { if
         * ((descriptionsGrid[x][y]!=null) &&
         * (descriptionsGrid[x][y].length()!=0)) {
         * //label.setText(descriptionsGrid[x][y]); label.setText(t+" - "+z); }
         * else { //label.setText(x+" - "+y); t = (e.getX()/100*2)-1 ; z =
         * (e.getY()/100*2)-1 ; label.setText(t+" - "+z); } }
         */
    }
    /*
     * public static void main(String[] args) { GridDrawer drawer = new
     * GridDrawer(5,5);
     *
     * drawer.drawColorAt(1, 1, Color.BLUE);
     *
     * JFrame frame = new JFrame(); frame.setContentPane(drawer.getDisplay());
     * frame.pack(); frame.setBounds(10, 10, 400, 400); frame.setVisible(true);
     * }
     */
}
