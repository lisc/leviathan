package fr.irstea.lisc.leviathan;


import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JComponent;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.View;
import org.graphstream.ui.swingViewer.Viewer;

/*
 * Copyright (C) 2014 dumoulin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *
 * @author dumoulin
 */
public class GraphDrawer {

    private Population population;
    private Graph graph;
    private Node[] nodes;
    private View view;
    private GUI2.ColorScale colorScale;

    public GraphDrawer(Population population, GUI2.ColorScale colorScale) {
        this.population = population;
        this.colorScale = colorScale;
        initNetworkGraph();
        Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_SWING_THREAD);
        viewer.addDefaultView(false);
        viewer.enableAutoLayout();
        view = viewer.getDefaultView();
        view.setBackground(new Color(0.8f,0.8f,0.8f));
        view.setMinimumSize(new Dimension(200, 200));
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");
        graph.addAttribute("ui.stylesheet", "graph { fill-color: rgb(200,200,200); } edge{arrow-shape:none;} node{size:20;}");
        update();
    }

    public JComponent getDisplay() {
        return view;
    }

    private Graph initNetworkGraph() {
        graph = new SingleGraph("Population");
        nodes = new Node[population.myPop.length];
        for (int i = 0; i < population.myPop.length; i++) {
            nodes[i] = graph.addNode("" + i);
        }
        return graph;
    }

    private String colorToCSS(Color color) {
        StringBuilder colorS = new StringBuilder("rgb(");
        colorS.append(color.getRed()).append(",").append(color.getGreen()).append(",").append(color.getBlue()).append(")");
        return colorS.toString();
    }

    public void update() {
        for (int i = 0; i < population.myPop.length; i++) {
            // style the node with reputation
            Color color = colorScale.getColor(Population.normalizeOpinion(population.getReputation(i)));
            nodes[i].addAttribute("ui.style", "fill-color: "+colorToCSS(color)+";");
            // update edges
            for (int j = 0; j < population.myPop.length; j++) {
                if (population.myPop[i].opinion[j] > 0.0f) { // conditions is a positive opinion of i for j
                    if (i != j) {
                        Edge edge = nodes[i].getEdgeToward(j + "");
                        if (edge == null) {
                            edge = graph.addEdge(i + "->" + j, "" + i, "" + j, true);
                        }
                        edge.addAttribute("opinion", population.myPop[i].opinion[j]);
                        color = colorScale.getColor(Population.normalizeOpinion(population.myPop[i].opinion[j]));
                        edge.addAttribute("ui.style", "fill-color:"+colorToCSS(color)+";");

                    }
                } else {
                    Edge edge = nodes[i].getEdgeToward(j + "");
                    if (edge != null) {
                        graph.removeEdge(edge);
                    }
                }
            }
        }
    }

}
