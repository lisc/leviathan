package fr.irstea.lisc.leviathan;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

public class LeviathanDemo {

    public static void main(String[] args) {
        int graine = 0;
        int ligne = 0;
        int compte = 0;
        int popSize = 40;
        int ka = 10;
        float delt = 0.2f;
        float sig = 0.5f;
        float ro = 0.8f;
        float we = 0.3f;
        int mem = 39;
        int heur = 2;
        int nbIter = 5000000;
        int freqSauvegarde = 1;
        int typeDiscus = 2;
        int typeRes = 1;
        int resM = 1;
        float noise = 0.0f;
        Parameters parameters = new Parameters(ligne, compte, popSize, ka, delt, sig, ro, we, mem, heur, nbIter, graine, freqSauvegarde, typeDiscus, typeRes, resM, noise);
        Population population = new Population(parameters);
        final GUI monGui = new GUI(population, "Leviathan Model");
        long nbIterat = (long) parameters.getNbIter() * ((long) popSize / (long) 2);
        Timer timer = new Timer(50, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                monGui.update();
            }

        });
        timer.start();
        for (long i = 1; i <= nbIterat; i++) {
            population.iteration_step(i);
        }

    }
}
