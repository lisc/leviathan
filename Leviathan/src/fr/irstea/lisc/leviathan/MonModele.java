package fr.irstea.lisc.leviathan;


import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.io.StreamTokenizer;

import cern.jet.random.Uniform;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import org.apache.commons.io.FileUtils;

/**
 * @author : Sylvie Huet
 * @version : juin 2005 Cette classe lance le mod�le individu centr� d'�tude de
 * la transmission d'information
 */
class MonModele {

    PrintStream console = System.out;
    PrintStream ps1;
    FileOutputStream fic1;
    PrintStream ps2;
    FileOutputStream fic2;
    PrintStream ps3;
    FileOutputStream fic3;
    PrintStream ps4;
    FileOutputStream fic4;
    PrintStream ps5;
    FileOutputStream fic5;
    PrintStream ps6;
    FileOutputStream fic6;
    PrintStream ps7;
    FileOutputStream fic7;
    PrintStream ps8;
    FileOutputStream fic8;
    PrintStream ps9;
    FileOutputStream fic9;
    static String nomFichier;
    /**
     * population ayant � d�cider sur l'objet
     */
    public Population Pop;

    /*
     * public static void main(String[] args) { nomFichier = args[0]; new
     * ModeleInfoTrans(nomFichier); }
     */
    public MonModele(String param, String nomFichR, String toto, String leader, String histogram, String avalanche, int ligne, boolean entete) {
        createOutputFile(nomFichR, toto, leader, histogram, avalanche);
        if (entete) {
            console.println("lancons la simulation avec le fichier " + param + " le fichier resultat est " + nomFichR);
        }
        lectureEntree(param, ligne, entete);
        try {
            fic1.close();
            ps1.close();
            fic2.close();
            ps2.close();
            fic3.close();
            ps3.close();
            fic4.close();
            ps4.close();
            fic5.close();
            ps5.close();
            fic6.close();
            ps6.close();
            fic7.close();
            ps7.close();
            fic8.close();
            ps8.close();
            fic9.close();
            ps9.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("probleme de fermeture du fichier");
        }
    }

    /**
     * Constructeur pour expérimenter en lots et sortie dans des fichiers conçu
     * pour créer une base de données
     */
    public MonModele(String param, int ligne, boolean entete, PrintStream prS1, PrintStream prS2, PrintStream prS3) {
        ps1 = prS1;
        ps2 = prS2;
        ps9 = prS3;
        lectureEntreeExpeLot(param, ligne, entete);
    }

    /**
     * Lecture des caract�ristiques de la population
     */
    public void lectureEntree(String fileName, int ligne, boolean entete) {
        //public void lectureEntree(String fileName, int ligne, int frequenceSauvegarde, boolean entete) {
        try {
            // ouverture du fichier.
            FileReader file = new FileReader(fileName);
            StreamTokenizer st = new StreamTokenizer(file);
            while (st.lineno() < ligne + 1) {
                st.nextToken();
            }
            int nbReplicats = (int) st.nval;
            st.nextToken();
            int nbIter = (int) st.nval;
            st.nextToken();
            int popSize = (int) st.nval;
            st.nextToken();
            int ka = (int) st.nval;
            st.nextToken();
            float delt = (float) st.nval;
            st.nextToken();
            float sig = (float) st.nval;
            st.nextToken();
            float ro = (float) st.nval;
            st.nextToken();
            float we = (float) st.nval;
            st.nextToken();
            int mem = (int) st.nval;
            st.nextToken();
            int heur = (int) st.nval;
            st.nextToken();
            ///*
            int typeRes = (int) st.nval;
            st.nextToken();
            int resM = (int) st.nval;
            st.nextToken();
            float noise = (float) st.nval;
            st.nextToken();
            int typeDiscus = (int) st.nval;
            st.nextToken();
            int freqSauvegarde = (int) st.nval;
            st.nextToken();
            /*
             * Check for N != k and m>1
             */
            if (ka >= popSize - 2) {
                System.err.println("Error: the number of acquaintances must be less than N-2, ka : " + ka + " popSize " + popSize);
                System.exit(0);
            }
            if (mem < 1) {
                System.err.println("Error: the memory has to be at least equal to 1");
                System.exit(0);
            }

            // Lancement des replicats
            int compte = 0;
            while (compte < nbReplicats) {
                System.err.println("Experimentation n° " + ligne + " replica n° " + compte);
                Pop = null;
                int graine = (Uniform.staticNextIntFromTo(0, Integer.MAX_VALUE) * (compte + 1) * 10000); // la graine varie en fonction du r�plicat
                if (entete & compte == 0) {
                    ps1.print("numSimu" + ";");
                    ps1.print("NumReplicat" + ";");
                    ps1.print("TaillePop" + ";");
                    ps1.print("NbIter" + ";");
                    ps1.print("k" + ";");
                    ps1.print("delta" + ";");
                    ps1.print("sigma" + ";");
                    ps1.print("ro" + ";");
                    ps1.print("we" + ";");
                    ps1.print("memory" + ";");
                    ps1.print("heuristic" + ";");
                    ps1.print("type Discuss" + ";");
                    ps1.print("type Res" + ";");
                    ps1.print("res param" + ";");
                    ps1.print("noise" + ";");
                    ps1.println("freqSav" + ";");

                    ps2.print("numSimu" + ";");
                    ps2.print("NumReplicat" + ";");
                    ps2.print("TaillePop" + ";");
                    ps2.print("NbIter" + ";");
                    ps2.print("k" + ";");
                    ps2.print("delta" + ";");
                    ps2.print("sigma" + ";");
                    ps2.print("ro" + ";");
                    ps2.print("we" + ";");
                    ps2.print("memory" + ";");
                    ps2.print("heuristic" + ";");
                    ps2.print("type Discuss" + ";");
                    ps2.print("type Res" + ";");
                    ps2.print("res param" + ";");
                    ps2.print("noise" + ";");
                    ps2.print("freqSav" + ";");
                    ps2.print("Iteration " + ";");
                    //ps2.print("NbLovedByEverybody " + ";");
                    ps2.print("selector" + ";");
                    ps2.print("dominance" + ";");
                    ps2.print("averageOp" + ";");
                    ps2.print("maxRep" + ";");
                    ps2.print("secMaxRep" + ";");
                    ps2.print("lastRep" + ";");
                    //ps2.print("v" + ";");
                    //ps2.print("rep Leader" + ";");
                    //ps2.print("2->1" + ";");
                    ps2.print("nondom-period" + ";");
                    ps2.println("dom-period" + ";");

                    ps3.print("numSimu" + ";");
                    ps3.print("NumReplicat" + ";");
                    ps3.print("TaillePop" + ";");
                    ps3.print("NbIter" + ";");
                    ps3.print("k" + ";");
                    ps3.print("delta" + ";");
                    ps3.print("sigma" + ";");
                    ps3.print("ro" + ";");
                    ps3.print("we" + ";");
                    ps3.print("memory" + ";");
                    ps3.print("heuristic" + ";");
                    ps3.print("type Discuss" + ";");
                    ps3.print("type Res" + ";");
                    ps3.print("res param" + ";");
                    ps3.print("noise" + ";");
                    ps3.println("freqSav" + ";");

                    ps4.print("numSimu" + ";");
                    ps4.print("NumReplicat" + ";");
                    ps4.print("TaillePop" + ";");
                    ps4.print("NbIter" + ";");
                    ps4.print("k" + ";");
                    ps4.print("delta" + ";");
                    ps4.print("sigma" + ";");
                    ps4.print("ro" + ";");
                    ps4.print("we" + ";");
                    ps4.print("memory" + ";");
                    ps4.print("heuristic" + ";");
                    ps4.print("type Discuss" + ";");
                    ps4.print("type Res" + ";");
                    ps4.print("res param" + ";");
                    ps4.print("noise" + ";");
                    ps4.println("freqSav" + ";");

                    ps5.print("numSimu" + ";");
                    ps5.print("NumReplicat" + ";");
                    ps5.print("TaillePop" + ";");
                    ps5.print("NbIter" + ";");
                    ps5.print("k" + ";");
                    ps5.print("delta" + ";");
                    ps5.print("sigma" + ";");
                    ps5.print("ro" + ";");
                    ps5.print("we" + ";");
                    ps5.print("memory" + ";");
                    ps5.print("heuristic" + ";");
                    ps5.print("type Discuss" + ";");
                    ps5.print("type Res" + ";");
                    ps5.print("res param" + ";");
                    ps5.print("noise" + ";");
                    ps5.println("freqSav" + ";");
                    //ps2.print("Iteration " + ";");
                    /**
                     * System.out.print("Total opinion mean" + ";");
                     * System.out.print("Total opinion min" + ";");
                     * System.out.print("Total opinion max" + ";");
                     * System.out.print("Nb positive" + ";");
                     * System.out.print("Nb negative" + ";");
                     * System.out.print("nbReciprocalLoveLinkCumul" + ";");
                     * System.out.print("nbReciprocalLoveLinkIfEgoC" + ";");
                     * System.out.print("nbReciprocalLoveLinkIfOtherC" + ";");
                     * System.out.print("nbReciprocalLoveLinkIfLeader" + ";");
                     * System.out.print("NbEgoCentered" + ";");
                     * System.out.print("NbOtherCentered" + ";");
                     * System.out.print("NbLeaders" + ";");
                     * System.out.print("OlderLeaderDuration" + ";");
                     * System.out.print("MostLoveLeaderDuration" + ";");
                     * System.out.print("LoveForOldestLeader" + ";");
                     * System.out.print("1st loved leader - love for" + ";");
                     * System.out.print("2nd loved leader - love for" + ";");
                     * System.out.print("3rd loved leader - love for" + ";");
                     * System.out.print("4th loved leader - love for" + ";");
                     * System.out.print("5th loved leader - love for" + ";");
                     * System.out.print("6th loved leader - love for" + ";");
                     * System.out.print("7th loved leader - love for" + ";");
                     * System.out.print("8th loved leader - love for" + ";");
                     * System.out.print("9th loved leader - love for" + ";");
                     * System.out.print("10th loved leader - love for" + ";");
                     * System.out.print("11th loved leader - love for" + ";");
                     * System.out.print("12th loved leader - love for" + ";");
                     * System.out.print("13th loved leader - love for" + ";");
                     * System.out.print("14th loved leader - love for" + ";");
                     * System.out.print("15th loved leader - love for" + ";");
                     * System.out.print("16th loved leader - love for" + ";");
                     * System.out.print("17th loved leader - love for" + ";");
                     * System.out.print("18th loved leader - love for" + ";");
                     * System.out.print("19th loved leader - love for" + ";");
                     * System.out.print("20th loved leader - love for" + ";");
                     * //System.out.print("PartPos" + ";");
                     * //System.out.print("NbLovedIndivAverage" + ";");
                     * //System.out.print("DifLoveBetFirstandSecondLeader" +
                     * ";"); //System.out.print("DifLoveBeSecondandThirdLeaderr"
                     * + ";"); System.out.print("Dif selfEsteem-reputation -0,9"
                     * + ";"); System.out.print("Dif selfEsteem-reputation -0,8"
                     * + ";"); System.out.print("Dif selfEsteem-reputation -0,7"
                     * + ";"); System.out.print("Dif selfEsteem-reputation -0,6"
                     * + ";"); System.out.print("Dif selfEsteem-reputation -0,5"
                     * + ";"); System.out.print("Dif selfEsteem-reputation -0,4"
                     * + ";"); System.out.print("Dif selfEsteem-reputation -0,3"
                     * + ";"); System.out.print("Dif selfEsteem-reputation -0,2"
                     * + ";"); System.out.print("Dif selfEsteem-reputation -0,1"
                     * + ";"); System.out.print("Dif selfEsteem-reputation 0" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 0,1" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 0,2" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 0,3" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 0,4" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 0,5" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 0,6" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 0,7" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 0,8" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 0,9" +
                     * ";"); System.out.print("Dif selfEsteem-reputation 1,0" +
                     * ";"); System.out.print("selfEsteem density -0,9" + ";");
                     * System.out.print("selfEsteem density -0,8" + ";");
                     * System.out.print("selfEsteem density -0,7" + ";");
                     * System.out.print("selfEsteem density -0,6" + ";");
                     * System.out.print("selfEsteem density -0,5" + ";");
                     * System.out.print("selfEsteem density -0,4" + ";");
                     * System.out.print("selfEsteem density -0,3" + ";");
                     * System.out.print("selfEsteem density -0,2" + ";");
                     * System.out.print("selfEsteem density -0,1" + ";");
                     * System.out.print("selfEsteem density 0" + ";");
                     * System.out.print("selfEsteem density 0,1" + ";");
                     * System.out.print("selfEsteem density 0,2" + ";");
                     * System.out.print("selfEsteem density 0,3" + ";");
                     * System.out.print("selfEsteem density 0,4" + ";");
                     * System.out.print("selfEsteem density 0,5" + ";");
                     * System.out.print("selfEsteem density 0,6" + ";");
                     * System.out.print("selfEsteem density 0,7" + ";");
                     * System.out.print("selfEsteem density 0,8" + ";");
                     * System.out.print("selfEsteem density 0,9" + ";");
                     * System.out.print("selfEsteem density 1,0" + ";");
                     * System.out.print("reputation density -0,9" + ";");
                     * System.out.print("reputation density -0,8" + ";");
                     * System.out.print("reputation density -0,7" + ";");
                     * System.out.print("reputation density -0,6" + ";");
                     * System.out.print("reputation density -0,5" + ";");
                     * System.out.print("reputation density -0,4" + ";");
                     * System.out.print("reputation density -0,3" + ";");
                     * System.out.print("reputation density -0,2" + ";");
                     * System.out.print("reputation density -0,1" + ";");
                     * System.out.print("reputation density 0" + ";");
                     * System.out.print("reputation density 0,1" + ";");
                     * System.out.print("reputation density 0,2" + ";");
                     * System.out.print("reputation density 0,3" + ";");
                     * System.out.print("reputation density 0,4" + ";");
                     * System.out.print("reputation density 0,5" + ";");
                     * System.out.print("reputation density 0,6" + ";");
                     * System.out.print("reputation density 0,7" + ";");
                     * System.out.print("reputation density 0,8" + ";");
                     * System.out.print("reputation density 0,9" + ";");
                     * System.out.print("reputation density 1,0" + ";");
                     */
                    System.out.println();
                }
                Parameters parameters = new Parameters(ligne, compte, popSize, ka, delt, sig, ro, we, mem, heur, nbIter, graine, freqSauvegarde, typeDiscus, typeRes, resM, noise);
                Pop = new Population(parameters, this);
                compte++;
            }
            System.err.println();
        } catch (Exception e) {
            System.err.println("erreur lecture : " + e.toString());
            e.printStackTrace();
            System.out.println("erreur lecture : " + e.toString());
        }
    }

    /**
     * Lecture des caract�ristiques de la population
     */
    public void lectureEntreeExpeLot(String fileName, int ligne, boolean entete) {
        //public void lectureEntree(String fileName, int ligne, int frequenceSauvegarde, boolean entete) {
        try {
            // ouverture du fichier.
            FileReader file = new FileReader(fileName);
            StreamTokenizer st = new StreamTokenizer(file);
            while (st.lineno() < ligne + 1) {
                st.nextToken();
            }
            int nbReplicats = (int) st.nval;
            st.nextToken();
            int nbIter = (int) st.nval;
            st.nextToken();
            int popSize = (int) st.nval;
            st.nextToken();
            int ka = (int) st.nval;
            st.nextToken();
            float delt = (float) st.nval;
            st.nextToken();
            float sig = (float) st.nval;
            st.nextToken();
            float ro = (float) st.nval;
            st.nextToken();
            float we = (float) st.nval;
            st.nextToken();
            int mem = (int) st.nval;
            st.nextToken();
            int heur = (int) st.nval;
            st.nextToken();
            ///*
            int typeRes = (int) st.nval;
            st.nextToken();
            int resM = (int) st.nval;
            st.nextToken();
            float noise = (float) st.nval;
            st.nextToken();
            int typeDiscus = (int) st.nval;
            st.nextToken();
            int freqSauvegarde = (int) st.nval;
            st.nextToken();

            /*
             * Check for N != k and m>1
             */
            if (ka >= popSize - 2) {
                System.err.println("Error: the number of acquaintances must be less than N-2, ka : " + ka + " popSize " + popSize);
                System.exit(0);
            }
            if (mem < 1) {
                System.err.println("Error: the memory has to be at least equal to 1");
                System.exit(0);
            }

            //*/
            //System.out.println("Lecture des param�tres finie");
            // writing title pour opinions
            if (entete) {
                ps1.print("nameExpeLot" + ";");
                ps1.print("numSimu" + ";");
                ps1.print("NumReplicat" + ";");
                ps1.print("iteration" + ";");
                ps1.print("NumIndiv" + ";");
                for (int i = 0; i < popSize; i++) {
                    ps1.print("OpDeIndivSur" + i + ";");
                }
                ps1.println();
                // writing title for network of reciprocal positive links properties
                ps9.print("nameExpeLot" + ";");
                ps9.print("numSimu" + ";");
                ps9.print("NumReplicat" + ";");
                ps9.print("TaillePop" + ";");
                ps9.print("NbIter" + ";");
                ps9.print("k" + ";");
                ps9.print("delta" + ";");
                ps9.print("sigma" + ";");
                ps9.print("ro" + ";");
                ps9.print("we" + ";");
                ps9.print("memory" + ";");
                ps9.print("heuristic" + ";");
                ps9.print("typeDiscuss" + ";");
                ps9.print("typeRes" + ";");
                ps9.print("resParam" + ";");
                ps9.print("noise" + ";");
                ps9.print("freqSauvegarde" + ";");
                ps9.print("iteration" + ";");
                ps9.print("consensus sur op" + ";");
                ps9.print("somme pente distrib reput" + ";");
                ps9.print("min pente distrib reput" + ";");
                ps9.print("max pente distrib reput" + ";");
                ps9.print("typeOfNetwork" + ";");
                ps9.print("nbComposantesConnexes" + ";");
                ps9.print("averageDegree" + ";" + "minDegree" + ";" + "maxDegree" + ";");
                ps9.print("averageShortPathLength" + ";" + "minShortPathLength" + ";" + "maxShortPathLength" + ";");
                ps9.print("averageClusterCoeff" + ";" + "minClusterCoeff" + ";" + "maxClusterCoeff" + ";");
                ps9.print("nbLinks" + ";" + "nbRecLinks"+ ";");
                ps9.print("pattern"+ ";");
                ps9.println("degreeDistribution");
            }
            // Lancement des replicats
            int compte = 0;
            while (compte < nbReplicats) {
                System.err.println("Experimentation n° " + ligne + " replica n° " + compte);
                Pop = null;
                int graine = (Uniform.staticNextIntFromTo(0, Integer.MAX_VALUE) * (compte + 1) * 10000); // la graine varie en fonction du r�plicat
                if (entete & compte == 0) {
                    /*
                     * ps1.print("nameExpeLot" + ";"); ps1.print("numSimu" +
                     * ";"); ps1.print("NumReplicat" + ";");
                     * ps1.print("iteration" + ";"); ps1.print("NumIndiv" +
                     * ";"); for (int i = 0; i < popSize; i++) {
                     * ps1.print("OpDeIndivSur" + i + ";"); }
                     */
                    ps2.print("nameExpeLot" + ";");
                    ps2.print("numSimu" + ";");
                    ps2.print("NumReplicat" + ";");
                    ps2.print("TaillePop" + ";");
                    ps2.print("NbIter" + ";");
                    ps2.print("k" + ";");
                    ps2.print("delta" + ";");
                    ps2.print("sigma" + ";");
                    ps2.print("ro" + ";");
                    ps2.print("we" + ";");
                    ps2.print("memory" + ";");
                    ps2.print("heuristic" + ";");
                    ps2.print("typeDiscuss" + ";");
                    ps2.print("typeRes" + ";");
                    ps2.print("resParam" + ";");
                    ps2.print("noise" + ";");
                    ps2.print("freqSav" + ";");
                    ps2.print("iteration" + ";");
                    ps2.print("nbDominance" + ";");
                    ps2.print("nbHierarchy" + ";");
                    ps2.print("nbEgality" + ";");
                    ps2.print("nbElite" + ";");
                    ps2.println("nbCrisis" + ";");
                }
                Parameters parameters = new Parameters(ligne, compte, popSize, ka, delt, sig, ro, we, mem, heur, nbIter, graine, freqSauvegarde, typeDiscus, typeRes, resM, noise);
                Pop = new Population(fileName, parameters, this);
                compte++;
            }
            System.err.println();
        } catch (Exception e) {
            System.err.println("erreur lecture : " + e.toString());
            e.printStackTrace();
            System.out.println("erreur lecture : " + e.toString());
        }
    }

    /**
     * m�thode d'�criture des r�sultats de la simulation, on �crit la valeur des
     * objets du modele
     */
    public void createOutputFile(String nameResult, String toto, String leader, String histogram, String avalanche) {
        try {
            File file1 = new File(nameResult);
            File file2 = new File(toto);
            File file3 = new File(leader);
            File file4 = new File(histogram);
            File file5 = new File(avalanche);
            File file6 = new File("self-esteemd4.txt");
            File file7 = new File("reputationd4.txt");
            File file8 = new File("se-rep4.txt");
            File file9 = new File("reseauForce.csv");

            if (file1.exists()) {
                String msg = "<html><body>The output directory <b>" + file1.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            if (file2.exists()) {
                String msg = "<html><body>The output directory <b>" + file2.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            if (file3.exists()) {
                String msg = "<html><body>The output directory <b>" + file3.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            if (file4.exists()) {
                String msg = "<html><body>The output directory <b>" + file4.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            if (file5.exists()) {
                String msg = "<html><body>The output directory <b>" + file5.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }

            fic1 = new FileOutputStream(file1);
            ps1 = new PrintStream(fic1);
            fic2 = new FileOutputStream(file2);
            ps2 = new PrintStream(fic2);
            fic3 = new FileOutputStream(file3);
            ps3 = new PrintStream(fic3);
            fic4 = new FileOutputStream(file4);
            ps4 = new PrintStream(fic4);
            fic5 = new FileOutputStream(file5);
            ps5 = new PrintStream(fic5);
            fic6 = new FileOutputStream(file6);
            ps6 = new PrintStream(fic6);
            fic7 = new FileOutputStream(file7);
            ps7 = new PrintStream(fic7);
            fic8 = new FileOutputStream(file8);
            ps8 = new PrintStream(fic8);
            fic9 = new FileOutputStream(file9);
            ps9 = new PrintStream(fic9);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println();
            System.out.println("j'ai un probl�me d'�criture des r�sultats");
        }

    }

    /**
     * m�thode d'�criture des r�sultats de la simulation, on �crit la valeur des
     * objets du modele
     */
    public void createOutputFile(String nameResult, String toto, String nuts) {
        try {
            File file1 = new File(nameResult);
            File file2 = new File(toto);
            File file9 = new File(nuts);
            if (file1.exists()) {
                String msg = "<html><body>The output directory <b>" + file1.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            if (file2.exists()) {
                String msg = "<html><body>The output directory <b>" + file2.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            if (file9.exists()) {
                String msg = "<html><body>The output directory <b>" + file9.getAbsolutePath() + "</b> already exists!";
                if (GraphicsEnvironment.isHeadless()) {
                    System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                    System.exit(1);
                } else {
                    JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                    // set non-bold font
                    Font font = UIManager.getFont("OptionPane.font");
                    label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                    if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            }
            fic1 = new FileOutputStream(file1);
            ps1 = new PrintStream(fic1);
            fic2 = new FileOutputStream(file2);
            ps2 = new PrintStream(fic2);
            fic9 = new FileOutputStream(file9);
            ps9 = new PrintStream(fic9);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println();
            System.out.println("j'ai un probl�me d'�criture des r�sultats");
        }

    }

    public static File createOutputDir(String dirname) throws IOException, HeadlessException {
        File outputDir = new File(dirname);
        if (outputDir.exists()) {
            String msg = "<html><body>The output directory <b>" + outputDir.getAbsolutePath() + "</b> already exists!";
            if (GraphicsEnvironment.isHeadless()) {
                System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                System.exit(1);
            } else {
                JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                // set non-bold font
                Font font = UIManager.getFont("OptionPane.font");
                label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    FileUtils.deleteDirectory(outputDir);
                }
            }
        }
        outputDir.mkdirs();
        return outputDir;
    }
}
