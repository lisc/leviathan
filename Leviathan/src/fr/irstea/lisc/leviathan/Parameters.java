package fr.irstea.lisc.leviathan;


import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.ohoui.annotations.SkipField;
import java.util.Locale;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dumoulin
 */
public class Parameters {

    @SkipField
    private int simu;
    @Description(name = "Number of replicates", tooltip="")
    private int rep;
    private int popSize;// size of the pop
    private int ka; // number of individuals an individuals talk about during an encounter
    private double delt; // bounds for random effect of the context of the encounter on the perception of the other
    @Description(name="sigma", tooltip = "parameter ruling the tendency to more being influenced by those having a better opinion of me than mine")
    private double sig; // parameter ruling the tendency to more being influenced by those having a better opinion of me than mine
    @Description(name="rho", tooltip = "parameter with sigma ruling the gossip")
    private double r; // parameter with sigma ruling the gossip
    @Description(name="omega",tooltip = "tendency to contrast the other (also called vanity) - that is a differentiation to other parameter")
    private double we; // tendency to contrast the other (also called vanity) - that is a differentiation to other parameter
    @SkipField
    private int mem;//size of known individuals
    private int heuristic;//heuristic chosen for the update
    private int nbIter;
    @SkipField
    private int graine;
    @SkipField
    private int freqSauvegarde;
    @SkipField
    private int typeDiscus;
    @SkipField
    private int typeRes;
    @SkipField
    private int resM;
    @SkipField
    private float noise;

    public Parameters(int simu, int rep, int popSize, int ka, float delt, float sig, float r, float we,
            int mem, int heuristic, int nbIter, int graine, int freqSauvegarde, int typeDiscus, int typeRes, int resM, float noise) {
        this.simu = simu;
        this.rep = rep;
        this.popSize = popSize;
        this.ka = ka;
        this.delt = delt;
        this.sig = sig;
        this.r = r;
        this.we = we;
        this.mem = mem;
        this.heuristic = heuristic;
        this.nbIter = nbIter;
        this.graine = graine;
        this.freqSauvegarde = freqSauvegarde;
        this.typeDiscus = typeDiscus;
        this.typeRes = typeRes;
        this.resM = resM;
        this.noise = noise;
    }

    public String getValues(String separator) {
        StringBuilder buff = new StringBuilder();
        buff.append(simu).append(separator);
        buff.append(rep).append(separator);
        buff.append(popSize).append(separator);
        buff.append(ka).append(separator);
        buff.append(String.format(Locale.FRENCH, "%f", delt)).append(separator);
        buff.append(String.format(Locale.FRENCH, "%f", sig)).append(separator);
        buff.append(String.format(Locale.FRENCH, "%f", r)).append(separator);
        buff.append(String.format(Locale.FRENCH, "%f", we)).append(separator);
        buff.append(mem).append(separator);
        buff.append(heuristic).append(separator);
        buff.append(nbIter).append(separator);
        buff.append(graine).append(separator);
        buff.append(freqSauvegarde).append(separator);
        buff.append(typeDiscus).append(separator);
        buff.append(typeRes).append(separator);
        buff.append(resM).append(separator);
        buff.append(String.format(Locale.FRENCH, "%f", noise)).append(separator);
        return buff.toString();
    }
    
    public int getSimu() {
        return simu;
    }

    public int getRep() {
        return rep;
    }

    public int getPopSize() {
        return popSize;
    }

    public int getKa() {
        return ka;
    }

    public float getDelt() {
        return (float) delt;
    }

    public float getSig() {
        return (float) sig;
    }

    public float getR() {
        return (float) r;
    }

    public float getWe() {
        return (float) we;
    }

    public int getMem() {
        return mem;
    }

    public int getHeuristic() {
        return heuristic;
    }

    public int getNbIter() {
        return nbIter;
    }

    public int getGraine() {
        return graine;
    }

    public int getFreqSauvegarde() {
        return freqSauvegarde;
    }

    public int getTypeDiscus() {
        return typeDiscus;
    }

    public int getTypeRes() {
        return typeRes;
    }

    public int getResM() {
        return resM;
    }

    public float getNoise() {
        return noise;
    }

}
