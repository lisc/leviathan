package fr.irstea.lisc.leviathan;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.PrintStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Logger;
//import org.openide.util.Exceptions;

/**
 *
 * @author sylvie.huet
 */
public class DataAnalysis {

    boolean patternDiagnosis = true;
    // Attributes of the application
    File directory;
    FileOutputStream write;
    PrintStream writer;
    Scanner scanner;
    Scanner scannerParam;
    //String rep = new String("C:/Users/sylvie.huet/Documents/Leviathan/opDataN10k2s03.txt");
    String rep = new String("C:/Users/sylvie.huet/Documents/LeviathanTravail/resultatsDeBaseLeviathan/opDataN10k1s05.txt");
    //String rep = new String("C:/Users/sylvie.huet/Documents/NetBeansProjects/Leviathan/opDataN100k10s03rhoPetit.txt");
    //String r = new String("N100k10s03rhoPetit");
    String r = new String("N10k1s05");
    int sizeP = 10;
    int nbExpes = 481; // 481
    int nbParam = 17;
    int iterLimitAnalyse = 210100;
    boolean enteteEcrite = false;
    //String repDeBase = new String("C:/PRIMAAnalyse/FiredTransitAged0"); // root containing the results to read
    //String repDeBase = new String("C:/Users/sylvie.huet/Documents/NetBeansProjects/Leviathan/");
    String repDeBase = new String("C:/Users/sylvie.huet/Documents/LeviathanTravail/resultatsDeBaseLeviathan/");
    float[][] opMatrix;
    String[][] param = new String[nbExpes][nbParam];
    /**
     * Average, min and max reputation
     */
    float averageOpinion;
    float maxOpinion;
    float minOpinion;
    float minReputation;
    float maxReputation;
    int indivMaxReput;
    int indivMinReput;
    float maxOpOnMaxReput;
    float minOpOnMaxReput;
    float maxOpOnMinReput;
    int nbCrisis;
    int nbElite;
    int nbHierarchy;
    int nbSmallWorld;
    int nbDominance;
    float secondMaxReputation;
    float diff;
    float dominance;
    float assymetry; //difference max between aij and aji
    float averageFriendNb;
    int leader;
    int previousLeader;
    int last; // individual with the weakest reputation
    int nbReputSup0;
    int nbReputSup025;
    int nbReputSup050;
    int nbReputSup075;
    float generalDistrustThreshold = -0.5f;
    boolean generalDistrust = false;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        new DataAnalysis();
    }

    public DataAnalysis() throws FileNotFoundException, IOException {
        opMatrix = new float[sizeP][sizeP];
        writer = null;
        System.err.println("Le fichier traité est: " + rep);
        directory = new File(repDeBase);
        String[] dirs = directory.list();
        //readAll(dirs, r + new String("dataAnalysis.csv"));
        readAll(dirs, r + new String("patternSynthesis.csv"));
        System.err.println("That's finished folks!");
        writer.close();
    }

    /**
     * Read all the results contains in the directory given as argument
     *
     * @param dirs
     * @param nbExpes
     */
    public void readAll(String[] dirs, String zen) {

        try {
            readParam();
            if (writer == null) {
                write = new FileOutputStream(new File(zen));
                writer = new PrintStream(write);
                if (patternDiagnosis) {
                    //writer.println("ExpesLot;"
                            //+ "nameExpeLot;numSimu;NumReplicat;TaillePop;NbIter;k;delta;sigma;ro;we;memory;HeuristicSupInMem;typeDiscuss;typeRes;resParam;noise;freqSav;"
                            //+ "iteration;pattern" + "");
                    writer.println("ExpesLot;"
                            + "nameExpeLot;numSimu;NumReplicat;TaillePop;NbIter;k;delta;sigma;ro;we;memory;HeuristicSupInMem;typeDiscuss;typeRes;resParam;noise;freqSav;"
                            + "iteration;nbDominance;nbHierarchy;nbSmallWorld;nbElite;nbCrisis" + "");
                } else {
                    writer.println("ExpesLot;"
                            + "nameExpeLot;numSimu;NumReplicat;TaillePop;NbIter;k;delta;sigma;ro;we;memory;HeuristicSupInMem;typeDiscuss;typeRes;resParam;noise;freqSav;"
                            + "iteration;minReputation;maxReputation;AverageOpinion;Dominance;"
                            + "MinOpinion;MaxOpinion;GeneralDistrust;AverageSelfBiais;"
                            + "assymetryMax;consensusLevel;averageNbFriends;"
                            + "nbReputSup0;nbReputSup025;nbReputSup050;nbReputSup075;Leader"
                            + "");
                }
            }
        } catch (Exception e) {
            System.err.println("Impossible d'écrire fichier de sortie " + write.toString());
        }
        readResults(dirs);
    }

    /**
     * Read all the results contains in the directory given as argument
     *
     * @param dirs
     * @param nbExpes
     */
    public void readParam() {
        String repParam = repDeBase.concat("param" + r + ".txt");
        int i = 0;
        try {
            scannerParam = new Scanner(new File(repParam));
            scannerParam.nextLine();
            // avoid the title line
            while (scannerParam.hasNextLine()) {
                param[i] = new String[nbParam];
                Arrays.fill(param[i], new String());
                param[i] = scannerParam.nextLine().split("\\t");
                i++;
            }
            scannerParam.close();
        } catch (Exception ex) {
            System.err.println("Erreur dans lecture parametres expe " + ex + " " + repParam);
        }
    }

    public void readResults(String[] dirs) {
        String expeNum = new String();
        String namePlanExpe = new String();
        String currentExpe = new String("");
        boolean begin = true;
        int iter = - 1;
        int indiv = -1;
        try {
            scanner = new Scanner(new File(rep));
            // read the head line
            scanner.nextLine();
            // read the data
            String[] cols;
            while (scanner.hasNextLine()) {
                cols = scanner.nextLine().split("\\t");
                //System.err.println(Arrays.asList(cols));
                namePlanExpe = cols[0];
                if (!cols[0].isEmpty()) {
                    expeNum = cols[1];
                } else {
                    expeNum = "-1";
                }
                if (begin) {
                    currentExpe = expeNum;
                }
                if (expeNum.equalsIgnoreCase(currentExpe)) {
                    begin = false;
                    indiv = (int) (new Float(cols[4]).floatValue());
                    iter = (int) (new Float(cols[3]).floatValue());
                    if (((int) new Float(iter).floatValue()) < iterLimitAnalyse) {
                        if (indiv == sizeP - 1) {
                            begin = false;
                            //System.err.println(" change matrix A ") ;
                            for (int j = 5; j < cols.length; j++) {
                                opMatrix[indiv][j - 5] = new Float(cols[j].replace(",", ".")).floatValue();
                            }
                            // analyse de la matrice stockée
                            analysisAndWrite(expeNum, namePlanExpe, iter, false);
                            opMatrix = new float[sizeP][sizeP];
                        } else {
                            for (int j = 5; j < cols.length; j++) {
                                opMatrix[indiv][j - 5] = new Float(cols[j].replace(",", ".")).floatValue();
                            }
                        }
                    }
                } else {
                    writePatternDiagnosis(currentExpe, namePlanExpe, iter, true);
                    nbCrisis = 0;
                    nbElite = 0;
                    nbHierarchy = 0;
                    nbSmallWorld = 0;
                    nbDominance = 0;
                    // new Experiment
                    cols = scanner.nextLine().split("\\t");
                    namePlanExpe = cols[0];
                    expeNum = cols[1];
                    System.err.println("NEW EXPE " + expeNum + " previous " + currentExpe);
                    currentExpe = expeNum;
                    indiv = (int) (new Float(cols[4]).floatValue());
                    iter = (int) (new Float(cols[3]).floatValue());
                    if (((int) new Float(iter).floatValue()) < iterLimitAnalyse) {
                        if (indiv == sizeP - 1) {
                            for (int j = 5; j < cols.length; j++) {
                                opMatrix[indiv][j - 5] = new Float(cols[j].replace(",", ".")).floatValue();
                            }
                            // analyse de la matrice stockée
                            analysisAndWrite(expeNum, namePlanExpe, iter, true);
                            opMatrix = new float[sizeP][sizeP];
                        } else {
                            for (int j = 5; j < cols.length; j++) {
                                opMatrix[indiv][j - 5] = new Float(cols[j].replace(",", ".")).floatValue();
                            }
                        }
                    }
                }
            }
            scanner.close();
        } catch (Exception ex) {
            System.err.println("Erreur dans la lecture des matrices " + ex);
        }
    }

    public void analysisAndWrite(String expeNum, String namePlanExpe, int iteration, boolean expeSynthesis) {
        if (expeSynthesis) {
            writePatternDiagnosis(expeNum, namePlanExpe, iteration, false);
        } else {
            if (!patternDiagnosis) {
                getDominance();
                writeAnalysis(expeNum, namePlanExpe, iteration);
            } else {
                writePatternDiagnosis(expeNum, namePlanExpe, iteration, false);
            }
        }
    }

    public void writeAnalysis(String expeN, String namePlanExpe, int iterat) {
        writer.print(rep + ";");
        //writer.print(namePlanExpe + ";");
        int expeDescript = ((int) new Float(expeN).floatValue()) - 1;
        //writer.print(expeN.replace(".", ",") + ";");
        for (int i = 0; i < nbParam; i++) {
            writer.print(param[expeDescript][i] + ";");
        }
        writer.print(iterat + ";");
        writer.print(new Float(minReputation).toString().replace(".", ",") + ";");
        writer.print(new Float(maxReputation).toString().replace(".", ",") + ";");
        writer.print(new Float(averageOpinion).toString().replace(".", ",") + ";");
        writer.print(new Float(getDominance()).toString().replace(".", ",") + ";");
        writer.print(new Float(minOpinion).toString().replace(".", ",") + ";");
        writer.print(new Float(maxOpinion).toString().replace(".", ",") + ";");
        writer.print(generalDistrust + ";");
        writer.print(new Float(getAverageSelfBiais()).toString().replace(".", ",") + ";");
        writer.print(new Float(assymetry).toString().replace(".", ",") + ";");
        writer.print(new Float(getAverageConsensus()).toString().replace(".", ",") + ";");
        writer.print(new Float(averageFriendNb).toString().replace(".", ",") + ";");
        writer.print(nbReputSup0 + ";");
        writer.print(nbReputSup025 + ";");
        writer.print(nbReputSup050 + ";");
        writer.print(nbReputSup075 + ";");
        writer.print(leader + ";");
        writer.println();
    }

    /**
     * Give the reputation of the individual when everyone kwnows everybody
     */
    public float getReputation(int indiv) {
        float reput = 0.0f;
        for (int i = 0; i < sizeP; i++) {
            if (i != indiv) {
                reput = reput + opMatrix[i][indiv];
            }
        }
        return reput / ((float) sizeP - 1);
    }

    /**
     * Give the self biais average value
     */
    public float getAverageSelfBiais() {
        float averageSelfBiais = 0.0f;
        for (int i = 0; i < sizeP; i++) {
            averageSelfBiais = averageSelfBiais + getSelfBiais(i);
        }
        return averageSelfBiais / ((float) sizeP);
    }

    /**
     * Give the self biais of an individual
     */
    public float getSelfBiais(int indiv) {
        return (Math.abs(opMatrix[indiv][indiv] - getFeelOthers(indiv)));
    }

    /**
     * Give the average consensus around reputation
     */
    public float getAverageConsensus() {
        float averageConsensus = 0.0f;
        averageFriendNb = 0.0f;
        for (int i = 0; i < sizeP; i++) {
            averageConsensus = averageConsensus + getConsensus(i);
            averageFriendNb = averageFriendNb + (float) getNbOfFriends(i);
        }
        averageFriendNb = averageFriendNb / ((float) sizeP);
        return averageConsensus / ((float) sizeP);
    }

    /**
     * Give the number of friends of an indiv
     */
    public int getNbOfFriends(int indiv) {
        int nbFriends = 0;
        for (int i = 0; i < sizeP; i++) {
            if (i != indiv) {
                if (opMatrix[indiv][i] > 0.0f) {
                    nbFriends++;
                }
            }
        }
        return nbFriends;
    }

    /**
     * Give the consensus around an indiv
     */
    public float getConsensus(int indiv) {
        return (Math.abs(opMatrix[indiv][indiv] - getReputation(indiv)));
    }

    /**
     * Get the average opinion of known individuals by an indiv
     */
    public float getFeelOthers(int indiv) {
        float feelOthers = 0.0f;
        for (int i = 0; i < sizeP; i++) {
            if (i != indiv) {
                feelOthers = feelOthers + opMatrix[indiv][i];
            }
        }
        return feelOthers / (float) (sizeP - 1);
    }

    public void findMaxReputation() {
        nbReputSup0 = 0;
        nbReputSup025 = 0;
        nbReputSup050 = 0;
        nbReputSup075 = 0;
        float reputation;
        maxReputation = -1;
        for (int i = 0; i < sizeP; i++) {
            reputation = getReputation(i);
            if (reputation > maxReputation) {
                maxReputation = reputation;
                leader = i;
                indivMaxReput = i;
            }
            if (reputation > 0.0f) {
                nbReputSup0++;
            }
            if (reputation > 0.25f) {
                nbReputSup025++;
            }
            if (reputation > 0.50f) {
                nbReputSup050++;
            }
            if (reputation > 0.75f) {
                nbReputSup075++;
            }
        }
    }

    public void findMinReputation() {
        float reputation;
        minReputation = 1;
        for (int i = 0; i < sizeP; i++) {
            reputation = getReputation(i);
            if (reputation < minReputation) {
                minReputation = reputation;
                last = i;
                indivMinReput = i;
            }
        }
    }

    public void findAverageOpinion() {
        averageOpinion = 0;
        minOpinion = 1.0f;
        maxOpinion = -1.0f;
        int count = 0;
        assymetry = 0.0f; // attention c'est l'assymétrie, la symétrie est la plus forte en 0
        float truc = 0.0f;
        for (int i = 0; i < sizeP; i++) {
            for (int j = 0; j < sizeP; j++) {
                truc = Math.abs(opMatrix[i][j] - opMatrix[j][i]);
                if (truc > assymetry) {
                    assymetry = truc;
                }
                if (minOpinion > opMatrix[i][j]) {
                    minOpinion = opMatrix[i][j];
                }
                if (maxOpinion < opMatrix[i][j]) {
                    maxOpinion = opMatrix[i][j];
                }
                averageOpinion = averageOpinion + opMatrix[i][j];
                count++;
            }
        }
        averageOpinion = averageOpinion / (float) (count);
        if (maxOpinion < generalDistrustThreshold) {
            generalDistrust = true;
        } else {
            generalDistrust = false;
        }
    }

    /*
     * Leader detection
     */
    /*
     * public void checkLeadership() { findMaxReputation(); if (leader > -1) {
     * // there's a leader if (previousLeader > -1) { // there's a previous
     * leader if (previousLeader == leader) { countLeadership++; } else { // new
     * leader writeLeadership = true; wLead = countLeadership; countLeadership =
     * 1; previousLeader = leader; } } else { // there's no previous leader
     * countLeadership = 1; previousLeader = leader; } } else { // there's no
     * leader if (previousLeader > -1) { // there's a previous leader
     * writeLeadership = true; wLead = countLeadership; countLeadership = 0;
     * previousLeader = -1; } } //System.err.println("prevLeader
     * "+previousLeader+" leader "+leader+" time leader "+countLeadership+"
     * previous time leader "+wLead) ; } /* /** Calculate dominance
     */
    public float getDominance() {
        findMaxReputation();
        findMinReputation();
        findAverageOpinion();
        dominance = maxReputation - averageOpinion - ((maxReputation - minReputation) / 2);
        //System.err.println("dominance " + dominance + " max " + maxReputation + " min " + minReputation + " average " + averageOpinion);
        return dominance;
    }

    public void OpOnHighestAndLowest() {
        maxOpOnMaxReput = -1.0f;
        minOpOnMaxReput = 1.0f;
        maxOpOnMinReput = -1.0f;
        for (int i = 0; i < sizeP; i++) {
            if (minOpOnMaxReput > opMatrix[i][indivMaxReput]) {
                minOpOnMaxReput = opMatrix[i][indivMaxReput];
            }
            if (maxOpOnMaxReput < opMatrix[i][indivMaxReput]) {
                maxOpOnMaxReput = opMatrix[i][indivMaxReput];
            }
            if (maxOpOnMinReput < opMatrix[i][indivMinReput]) {
                maxOpOnMinReput = opMatrix[i][indivMinReput];
            }
        }
    }

    public int getNbReputSupHighestAThreshold(float threshold) {
        int nb = 0;
        for (int i = 0; i < sizeP; i++) {
            if (getReputation(i) > threshold) {
                nb++;
            }
        }
        return nb;
    }

    public void writePatternDiagnosis(String expeN, String namePlanExpe, int iterat, boolean onlySynthesis) {
        //String pat = patternDiagnosis() ;
        if (!onlySynthesis) {
            String pat = patternDiagnosis();
            /*
             * writer.print(rep + ";"); int expeDescript = ((int) new
             * Float(expeN).floatValue()) - 1; for (int i = 0; i < nbParam; i++)
             * { writer.print(param[expeDescript][i] + ";"); }
             * writer.print(iterat + ";"); writer.print(pat + ";");
             * writer.println();
             *
             */
        } else {
            writer.print(rep + ";");
            int expeDescript = ((int) new Float(expeN).floatValue()) - 1;
            for (int i = 0; i < nbParam; i++) {
                writer.print(param[expeDescript][i] + ";");
            }
            writer.print(iterat + ";");
            writer.print(nbDominance + ";");
            writer.print(nbHierarchy + ";");
            writer.print(nbSmallWorld + ";");
            writer.print(nbElite + ";");
            writer.print(nbCrisis + ";");
            writer.println();
        }
    }

    public String patternDiagnosis() {
        // we have 1 = crisis ; 2 = dominance ; 3 = hierarchy ; 4 = smallworld ; 5 = elite ; 0 is not identified pattern
        String pat = new String("nothing");
        findMaxReputation();
        findMinReputation();
        OpOnHighestAndLowest();
        if (maxReputation < 0) {
            if (maxOpOnMaxReput > 0.5f) {
                if (maxOpOnMinReput > 0.0f) {
                    nbSmallWorld++;
                    return new String("small world");
                }
                nbElite++;
                return new String("elite");
            }
            nbCrisis++;
            return new String("crisis");
        } else {
            if (minOpOnMaxReput < -0.5f) {
                nbElite++;
                return new String("elite");
            }
            if (getNbReputSupHighestAThreshold((maxReputation - 0.5f)) < 3) {
                nbDominance++;
                return new String("dominance");
            }
            nbHierarchy++;
            return new String("hierarchy");
        }
    }
}
